## BodyHack API Design Endpoints


# Authentication:

* Signup: POST - /signup
    ### Create member account
    * Endpoint path: /api/accounts
    * Endpoint method: POST

    * Headers:
        * Authorization: Bearer token

    * Request shape (JSON):
        ```json {
                    "email": str,
                    "password": str,
                    "first_name": str,
                    "last_name": str
                }
        ```
    * Response: a new account
    * Response shape (JSON):
        ```json
        {
            "access_token": "string",
            "token_type": "Bearer",
            "account": {
                "id": 0,
                "email": "string"
                }
        }
        ```

* Login: POST - /token
    ### Log in

    * Endpoint path: /token
    * Endpoint method: POST

    * Request shape (form):
        * username: string
        * password: string

    * Response: Account information and a token
    * Response shape (JSON):
        ```json
        {
            "account": {
                "email": str,
                        "first_name": str,
                        "last_name": str,
            },
            "token": string
        }
        ```

* Logout: DELETE - /token
    ### Log out

    * Endpoint path: /token
    * Endpoint method: DELETE

    * Headers:
        * Authorization: Bearer token

    * Response: Always true
    * Response shape (JSON):
        ```json
        true
        ```


# Members:

* GET - /members
    ### Get list of members

    * Endpoint path: /members
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a list of members
    * Response shape (JSON):
        ```json
        {
            "members": [
                {
                    "id": 0,
                    "first_name": "string",
                    "last_name": "string",
                    "email": "string",
                    "role": "string"
                }
            ]
        }
        ```

* GET - /members/{member_id}
    ### Get a single member

    * Endpoint path: /members/{member_id}
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a member
    * Response shape (JSON):
        ```json
        {
            "id": 0,
            "first_name": "string",
            "last_name": "string",
            "email": "string",
            "role": "string"
        }
        ```

* POST - /api/accounts
    ### Create member (this actually happens in signup)

    * Endpoint path: /api/accounts
    * Endpoint method: POST

    * Headers:
        * Authorization: Bearer token

    * Request shape (JSON):
        ```json
        {
            "first_name": str,
            "last_name": str,
            "email": str
            "password": str
        }
        ```
    * Response: created member
    * Response shape (JSON):
        ```json
        {
            "access_token": "string",
            "token_type": "Bearer",
            "account": {
                "id": 0,
                "email": "string"
            }
        }
        ```

* PUT - /members/{member_id}
    ### Update member

    * Endpoint path: /members/{member_id}
    * Endpoint method: PUT

    * Headers:
        * Authorization: Bearer token

    * Request shape (JSON):
        ```json
        {
            "first_name": str,
            "last_name": str,
            "email": str
        }
        ```
    * Response: the updated record
    * Response shape (JSON):
        ```json
        {
            "id": 0,
            "first_name": "string",
            "last_name": "string",
            "email": "string",
            "role": "string"
        }
        ```

* DELETE -/members/{id}
    ### Delete member

    * Endpoint path: /members/{member_id}
    * Endpoint method: DELETE

    * Headers:
        * Authorization: Bearer token

    * Response: A positive response
    * Response shape (JSON):
        ```json
        {
            true
        }
        ```

* PUT - /members/{member_id}/change-password
    ### Update member's password

    * Endpoint path: /members/{member_id}/change-password
    * Endpoint method: PUT

    * Headers:
        * Authorization: Bearer token

    * Response: Boolean
    * Response shape (JSON):
        ```json
        {
            true
        }
        ```

# Trainers:

* GET - /trainers
    ### Get list of trainers

    * Endpoint path: /trainers
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a list of trainers
    * Response shape (JSON):
        ```json
        {
            "trainers": [
                {
                    "id": 0,
                    "first_name": "string",
                    "last_name": "string",
                    "email": "string",
                    "role": "string"
                }
            ]
        }
        ```

* GET -/trainers/{trainer_id}
    ### Get a single trainer

    * Endpoint path: /trainers/{trainer_id}
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a specific trainer
    * Response shape (JSON):
        ```json
        {
            "id": 0,
            "first_name": "string",
            "last_name": "string",
            "email": "string",
            "role": "string"
        }
        ```

* POST - /api/accounts/trainers
    ### Create trainer

    * Endpoint path: /api/accounts/trainers
    * Endpoint method: POST

    * Headers:
        * Authorization: Bearer token

    * Request shape (JSON):
        ```json
        {
            "email": "string",
            "password": "string",
            "first_name": "string",
            "last_name": "string"
        }
        ```
    * Response: created trainer
    * Response shape (JSON):
        ```json
        {
            "access_token": "string",
            "token_type": "Bearer",
            "account": {
                "id": 0,
                "email": "string"
            }
        }
        ```


# Appointments:

* GET from Trainer - /trainer/mine/appointments
    ### Get a list of Appointments

    * Endpoint path: /trainer/mine/appointments
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a list of Appointments
    * Response shape (JSON):
        ```json
        {
            "appointments": [
                {
                    "appointment_id": 0,
                    "date": "2024-01-31",
                    "time": "string",
                    "comments": "string",
                    "trainer_id": 0,
                    "member_id": 0,
                    "status": "string"
                }
            ]
        }
        ```

* GET from Member - /member/mine/appointments
    ### Get a list of Appointments

    * Endpoint path: /member/mine/appointments
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a list of Appointments
    * Response shape (JSON):
        ```json
        {
            "appointments": [
                {
                    "appointment_id": int,
                    "date": date,
                    "time": "string",
                    "comments": "string",
                    "trainer_id": int,
                    "member_id": int,
                    "status": "string"
                }
            ]
        }
        ```


* GET from Member - /member/mine/appointments/{appointment_id}
    ### Get a single appointment

    * Endpoint path: /member/mine/appointments/{appointment_id}
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: an appointment
    * Response shape (JSON):
        ```json
        {
            {
                "appointment_id": int,
                "date": "date",
                "time": "string",
                "comments": "string",
                "trainer_id": int,
                "member_id": int,
                "status": "string"
            }
        }
        ```

* POST - /members/mine/appointments
    ### Create an appointment

    * Endpoint path: /members/mine/appointments
    * Endpoint method: POST

    * Headers:
        * Authorization: Bearer token

    * Request body:
        ```json
        {
            {
                "date": datetime,
                "time": datetime,
                "trainer_id": FK Trainer,
                "member_id": FK Member,
                "comments": string
            }
        }
        ```

    * Response: The new record
    * Response shape:
        ```json
        {
            "appointment_id": int,
            "date": date,
            "time": "string",
            "comments": "string",
            "trainer_id": int,
            "member_id": int,
            "status": "string"
        }
        ```

* PUT - /members/mine/appointments/{appointment_id}
    ### Update an Appointment

    * Endpoint path: /members/mine/appointments/{appointment_id}
    * Endpoint method: PUT

    * Headers:
        * Authorization: Bearer token

    * Request body:
        ```json
        {
            "date": datetime,
            "time": datetime,
            "comments": string,
        }
        ```

    * Response: The updated record
    * Response shape:
        ```json
        {
            "appointment_id": 0,
            "date": "2024-01-31",
            "time": "string",
            "comments": "string",
            "trainer_id": 0,
            "member_id": 0,
            "status": "string"
        }
        ```

* PUT - /trainer/mine/appointments/{appointment_id}
    ### Update an Appointment

    * Endpoint path: /trainer/mine/appointments/{appointment_id}
    * Endpoint method: PUT

    * Headers:
        * Authorization: Bearer token

    * Request body:
        ```json
        {
            "date": datetime,
            "time": datetime,
            "comments": string,
        }
        ```

    * Response: The updated record
    * Response shape:
        ```json
        {
            "appointment_id": int,
            "date": date,
            "time": "string",
            "comments": "string",
            "trainer_id": int,
            "member_id": int,
            "status": "string"
        }
        ```

* DELETE - /members/mine/appointments/{appointment_id}
    ### Delete an Appointment

    * Endpoint path: /members/mine/appointments/{appointment_id}
    * Endpoint method: DELETE

    * Headers:
        * Authorization: Bearer token

    * Response: A positive response
    * Response shape:
        ```json
        {
            true
        }
        ```

# Personal Workouts:

* GET - /workouts/mine
    ### Get a list of workouts

    * Endpoint path: /workouts/mine
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a list of workouts
    * Response shape (JSON):
        ```json
        {
            "personal_workouts": [
                {
                    "id": int pk,
                    "personal_or_trainer": varchar,
                    "member": FK member_id,
                    "trainer": FK trainer_id (optional),
                    "category": str,
                    "date": datetime,
                    "start_time": varchar,
                    "end_time": varchar,
                    "exercise": str,
                    "weight": int(optional),
                    "reps_or_time": str,
                    "sets": str
                }
            ]
        }
        ```

* GET - /workouts/mine/{id}
    ### Get a specific workout

    * Endpoint path: /workouts/mine/{pk}
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a specific workout
    * Response shape (JSON):
        ```json
        {
            "id": int,
            "category": "string",
            "personal_or_trainer": "string",
            "trainer": int,
            "date": date,
            "start_time": "string",
            "end_time": "string",
            "member_id": int
        }
        ```

* POST - /workouts/mine
    ### Create a new workout

    * Endpoint path: /workouts/mine
    * Endpoint method: POST

    * Headers:
        * Authorization: Bearer token

    * Request shape (JSON):
        ``` json
        {
            "category": "string",
            "personal_or_trainer": "string",
            "trainer": int,
            "date": date
            "start_time": "string",
            "end_time": "string",
            "member_id": int
        }
        ```

    * Response: the created workout
    * Response shape (JSON):
        ```json
        {
            "id": int,
            "category": "string",
            "personal_or_trainer": "string",
            "trainer": int,
            "date": date,
            "start_time": "string",
            "end_time": "string",
            "member_id": int
        }
        ```

* PUT - /workouts/mine/{id}
    ### Update a workout

    * Endpoint path: /workouts/mine/{id}
    * Endpoint method: PUT

    * Headers:
        * Authorization: Bearer token

    * Request shape (JSON):
        ```json
        {
            "category": "string",
            "personal_or_trainer": "string",
            "trainer": int,
            "date": date,
            "start_time": "string",
            "end_time": "string",
            "member_id": int
        }
        ```

    * Response: the updated workout
    * Response shape (JSON):
        ```json
        {
            "category": "string",
            "personal_or_trainer": "string",
            "trainer": int,
            "date": date,
            "start_time": "string",
            "end_time": "string",
            "member_id": int
        }
        ```

* DELETE - /workouts/mine/{id}
    ### Delete a workout

    * Endpoint path: /workouts/mine/{id}
    * Endpoint method: DELETE

    * Headers:
        * Authorization: Bearer token

    * Response: a positive response
    * Response shape (JSON):
        ```json
        {
            true
        }
        ```

# Circuits:

* GET - /workouts/mine/{workout_id}/circuits
    ### Get a list of Circuits

    * Endpoint path: workouts/mine/{workout_id}/circuits
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a list of Circuits
    * Response shape (JSON):
        ```json
        {
            "circuits": [
                {
                    "exercise": string,
                    "weight": string,
                    "reps_or_time": string,
                    "sets": int
                }
            ]
        }
        ```

* GET - /workouts/mine/{workout_id}/circuits/{id}
    ### Get one circuit

    * Endpoint path: workouts/mine/{workout_id}/circuits
    * Endpoint method: GET

    * Headers:
        * Authorization: Bearer token

    * Response: a list of Circuits
    * Response shape (JSON):
        ```json
        {
            "id": int,
            "exercise": "string",
            "weight": "string",
            "reps_or_time": "string",
            "workout_id": int,
            "sets": int
        }
        ```

* POST - /workouts/mine/{workout_id}/circuits
    ### Create a Circuit

    * Endpoint path: workouts/mine/{workout_id}/circuits
    * Endpoint method: POST

    * Headers:
        * Authorization: Bearer token

    * Request body:
        ```json
        {
            "exercise": "string",
            "weight": "string",
            "reps_or_time": "string",
            "workout_id": int,
            "sets": int
        }
        ```

    * Response: The new record
    * Response shape:
        ```json
        {
            "circuits": [
                {
                    "id": int,
                    "exercise": string,
                    "weight": string,
                    "reps_or_time": string,
                    "workout_id": int,
                    "sets": int
                }
            ]
        }
        ```

* PUT - /workouts/mine/{workout_id}/circuits/{id}
    ### Update a Circuit

    * Endpoint path: workouts/mine/{workout_id}/circuits/{id}
    * Endpoint method: PUT

    * Headers:
        * Authorization: Bearer token

    * Request body:
        ```json
        {
            "exercise": string,
            "weight": string,
            "reps_or_time": string,
            "workout_id": int,
            "sets": int
        }
        ```

    * Response: The updated record
    * Response shape:
        ```json
        {
            "id": 0,
            "exercise": "string",
            "weight": "string",
            "reps_or_time": "string",
            "workout_id": 0,
            "sets": 0
    	}
        ```

* DELETE - /workouts/mine/{workout_id}/circuits/{id}
    ### Delete a Circuit

    * Endpoint path:  workouts/mine/{workout_id}/circuits/{id}
    * Endpoint method: DELETE

    * Headers:
        * Authorization: Bearer token

    * Response: A positive response
    * Response shape:
        ```json
        {
            true
        }
        ```
