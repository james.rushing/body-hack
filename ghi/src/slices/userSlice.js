import { createSlice } from '@reduxjs/toolkit';

// Create a slice to store the id, role, and name
const userSlice = createSlice({
    name: 'user',
    initialState: {
        id: null,
        role: null,
        fullName: null,
    },
    reducers: {
        setId: (state, action) => {
            state.id = action.payload.id;
        },
        setFullName: (state, action) => {
            const firstName = action.payload.first_name;
            const lastName = action.payload.last_name;
            state.fullName = `${firstName} ${lastName}`;
        },
        setRole: (state, action) => {
            state.role = action.payload.role;
        },
        resetUser: (state) => {
            state.id = null;
            state.role = null;
            state.fullName = null;
        },
        resetFullName: (state) => {
            state.fullName = null;
        },
    },
});

export const { setId, setFullName, setRole, resetUser, resetFullName } = userSlice.actions;
export const selectRole = (state) => state.user.role;
export const selectFullName = (state) => state.user.fullName;
export const selectId = (state) => state.user.id;

export default userSlice.reducer;
