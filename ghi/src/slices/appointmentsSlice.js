import { createSlice } from '@reduxjs/toolkit';

const appointmentsSlice = createSlice({
    name: 'appointments',
    initialState: {
        appointments: [],
        loading: 'idle',
        error: null,
        memberAppointments: null,
    },
    reducers: {},
});

export default appointmentsSlice.reducer;
