import { createSlice } from '@reduxjs/toolkit';

const storedTrainerAppointments = localStorage.getItem('trainerAppointmentsStore')

const trainerAppointmentsStoreSlice = createSlice({
    name: 'trainerAppointmentsStore',
    initialState: storedTrainerAppointments || [],
    reducers: {
        setTrainerAppointmentsStore: (state, action) => {
            const newAppt = action.payload;
            localStorage.setItem('trainerAppointmentsStore', newAppt);
            return newAppt;
        },
    },
});

export const { setTrainerAppointmentsStore } = trainerAppointmentsStoreSlice.actions;
export default trainerAppointmentsStoreSlice.reducer;
