import { createSlice } from '@reduxjs/toolkit';


const storedId = localStorage.getItem('userId')

export const idSlice = createSlice({
    name: 'userId',
    initialState: storedId || '',
    reducers: {
        setId: (action) => {
            const newId = action.payload;
            localStorage.setItem('userId', newId);
            return newId;
        },
        resetId: () => {
            localStorage.removeItem('userId');
            return ''
        }
    }
})

export const { setId, resetId } = idSlice.actions;
export default idSlice.reducer;
