import { createSlice } from '@reduxjs/toolkit';

const storedRole = localStorage.getItem('userRole');

export const roleSlice = createSlice({
    name: 'role',
    initialState: storedRole || '',
    reducers: {
        setRole: (action) => {
            const newRole = action.payload;
            localStorage.setItem('userRole', newRole);
            return newRole;
        },
        resetRole: () => {
            localStorage.removeItem('userRole');
            return '';
        },
    },
});

export const { setRole, resetRole } = roleSlice.actions;

export default roleSlice.reducer;
