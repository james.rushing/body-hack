import { useAuthContext } from '@galvanize-inc/jwtdown-for-react';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useGetTokenQuery } from '../services/gettoken';
import { useLoginMutation } from '../services/loginmutation';
import { setId, setFullName, setRole } from '../slices/userSlice';

const LoginForm = () => {
    const [formData, setFormData] = useState({
        username: '',
        password: '',
    });
    const [submitted, setSubmitted] = useState(false);
    const [isLoading, setIsLoading] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const { token } = useAuthContext();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [login, { isSuccess, isError }] = useLoginMutation();
    const {
        data,
        refetch: refetchToken,
        isSuccess: getTokenSuccess,
    } = useGetTokenQuery();

    const handleChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setIsLoading(true);
        try {
            const serializedFormData = new URLSearchParams(formData);
            await login(serializedFormData);
        } catch (e) {
            console.error(e);
        }
        const form = e.target;
        form.reset();
    };

    useEffect(() => {
        if (isSuccess) {
            setSubmitted(true);
            setIsLoading(false);
        } else if (isError) {
            console.log('isError', isError);
            setSubmitted(true);
            setIsLoading(false);
        }
    }, [isSuccess, isError]);

    useEffect(() => {
        if (data && getTokenSuccess) {
            dispatch(setId({ id: data.account.id }));
            dispatch(setRole({ role: data.account.role }));
            dispatch(
                setFullName({
                    first_name: data.account.first_name,
                    last_name: data.account.last_name,
                })
            );
            if (data.account.role === 'member') {
                navigate('/member');
            } else {
                navigate('/trainer');
            }
        }
    }, [data, getTokenSuccess, dispatch]);

    useEffect(() => {
        const fetchRoleAndNavigate = async () => {
            try {
                await refetchToken();
            } catch (error) {
                console.error(error);
            }
        };

        if (submitted) {
            if (isSuccess) {
                fetchRoleAndNavigate();
            } else if (!isLoading) {
                setErrorMessage('Invalid username or password.');
            }
        }
    }, [submitted, isLoading, isSuccess]);

    return (
        <div className="member-background-container">
            <div className="content-container align-items-center">
                <div className="container login-container my-5 col-3">
                    <h5 className="card-title">Login</h5>
                    {errorMessage && !token && (
                        <p className="error-message">{errorMessage}</p>
                    )}
                    <div className="card-body">
                        <form onSubmit={(e) => handleSubmit(e)}>
                            <div className="form-floating">
                                <input
                                    name="username"
                                    value={formData.username}
                                    id="username"
                                    type="text"
                                    placeholder=""
                                    className="form-control"
                                    onChange={handleChange}
                                />
                                <label htmlFor="username">Username:</label>
                            </div>
                            <div className="form-floating">
                                <input
                                    name="password"
                                    value={formData.password}
                                    id="password"
                                    type="password"
                                    placeholder=""
                                    className="form-control"
                                    onChange={handleChange}
                                />
                                <label htmlFor="password">Password:</label>
                            </div>
                            <div className="text-center">
                                <button
                                    className="btn btn-custom"
                                    style={{ width: '75px' }}
                                    type="submit"
                                    value="Login"
                                >
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LoginForm;
