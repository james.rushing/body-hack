import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    useGetTrainerAppointmentsQuery,
    useUpdateAcceptMutation,
    useUpdateRejectMutation,
} from '../../services/appointments';
import { useGetMembersQuery } from '../../services/getmembers';
import { useGetTokenQuery } from '../../services/loginmutation';
import { selectFullName, selectRole } from '../../slices/userSlice';

export default function TrainerHome() {
    const userRole = useSelector(selectRole);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const fullName = useSelector(selectFullName);
    const [updateAccept, acceptResult] = useUpdateAcceptMutation();
    const [updateReject, rejectResult] = useUpdateRejectMutation();

    const handleUpdateAccept = async (appointmentId) => {
        await updateAccept(appointmentId);
    };

    if (acceptResult.isSuccess) {
        console.log('Appointment accepted.');
    } else if (acceptResult.isError) {
        console.log('Error in accepting appointment.');
    }

    const handleUpdateReject = async (appointmentId) => {
        await updateReject(appointmentId);
    };

    if (rejectResult.isSuccess) {
        console.log('Appointment rejected.');
    } else if (rejectResult.isError) {
        console.log('Error in rejecting appointment.');
    }

    const {
        data: trainerAppointments = [],
        isLoading: trainerAppointmentLoading,
        isSuccess: trainerAppointmentSuccess,
        isError: trainerAppointmentError,
    } = useGetTrainerAppointmentsQuery();

    const { data: members = [], isSuccess: membersSuccess } =
        useGetMembersQuery();

    // Check if there's a valid token:
    const { data: tokenData, isSuccess, isLoading } = useGetTokenQuery();

    useEffect(() => {
        if (!isLoading && isSuccess && tokenData === null) {
            navigate('/');
        } else {
            return;
        }
    }, [tokenData, isLoading, isSuccess]);

    return (
        <div className="trainer-background-container">
            <div className="content-container">
                <div className="">
                    <h1
                        style={{
                            textAlign: 'center',
                            color: '#290202',
                            textShadow: '1px 1px 3px #c0a25e',
                            color: '#F4EBD0',
                        }}
                    >
                        Welcome {fullName}!
                    </h1>
                    <div className="row mt-5 justify-content-center">
                        <div className="col-md-8">
                            {trainerAppointmentLoading && <div>Loading</div>}
                            {trainerAppointmentError && (
                                <div>Error fetching appointments</div>
                            )}
                            <div>
                                <div className="row">
                                    <div className="mx-auto">
                                        {trainerAppointmentSuccess &&
                                        membersSuccess ? (
                                            <div className="container">
                                                <h3
                                                    style={{
                                                        textAlign: 'center',
                                                        color: '#F4EBD0',
                                                    }}
                                                >
                                                    Appointment Requests
                                                </h3>
                                                <div className="row p-4">
                                                    <table className="table table-dark table-striped">
                                                        <thead className="thead-dark">
                                                            <tr
                                                                className="text-center"
                                                                style={{
                                                                    backgroundColor:
                                                                        '#181818',
                                                                    color: '#F4EBD0',
                                                                }}
                                                            >
                                                                <th>
                                                                    Client Name
                                                                </th>
                                                                <th>Date</th>
                                                                <th>Time</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {trainerAppointmentSuccess &&
                                                                trainerAppointments.appointments
                                                                    .filter(
                                                                        (
                                                                            appointment
                                                                        ) =>
                                                                            appointment.status ===
                                                                            'pending'
                                                                    )
                                                                    .map(
                                                                        (
                                                                            appointment
                                                                        ) => {
                                                                            const matchingMember =
                                                                                members.members.find(
                                                                                    (
                                                                                        member
                                                                                    ) =>
                                                                                        member.id ===
                                                                                        appointment.member_id
                                                                                );
                                                                            const memberName =
                                                                                matchingMember
                                                                                    ? `${matchingMember.first_name} ${matchingMember.last_name}`
                                                                                    : 'Member not found.';

                                                                            const appointmentDateTime =
                                                                                new Date(
                                                                                    `${appointment.date}T${appointment.time}`
                                                                                );

                                                                            const formatDate =
                                                                                new Date(
                                                                                    appointment.date.replace(
                                                                                        /-/g,
                                                                                        '/'
                                                                                    )
                                                                                );

                                                                            const formattedDate =
                                                                                formatDate.toLocaleDateString(
                                                                                    'en-US',
                                                                                    {
                                                                                        year: 'numeric',
                                                                                        month: 'short',
                                                                                        day: 'numeric',
                                                                                    }
                                                                                );

                                                                            const formattedTime =
                                                                                appointmentDateTime.toLocaleTimeString(
                                                                                    [],
                                                                                    {
                                                                                        hour: '2-digit',
                                                                                        minute: '2-digit',
                                                                                        hour12: true,
                                                                                    }
                                                                                );

                                                                            const endTime =
                                                                                new Date(
                                                                                    appointmentDateTime.getTime()
                                                                                );
                                                                            endTime.setHours(
                                                                                endTime.getHours() +
                                                                                    1
                                                                            );
                                                                            const formattedEndTime =
                                                                                endTime.toLocaleTimeString(
                                                                                    [],
                                                                                    {
                                                                                        hour: '2-digit',
                                                                                        minute: '2-digit',
                                                                                        hour12: true,
                                                                                    }
                                                                                );
                                                                            const timeRange = `${formattedTime} - ${formattedEndTime}`;

                                                                            return (
                                                                                <tr
                                                                                    className="text-center"
                                                                                    key={
                                                                                        appointment.appointment_id
                                                                                    }
                                                                                >
                                                                                    <td>
                                                                                        {
                                                                                            memberName
                                                                                        }
                                                                                    </td>
                                                                                    <td>
                                                                                        {
                                                                                            formattedDate
                                                                                        }
                                                                                    </td>
                                                                                    <td>
                                                                                        {
                                                                                            timeRange
                                                                                        }
                                                                                    </td>
                                                                                    <td>
                                                                                        <span
                                                                                            style={{
                                                                                                display:
                                                                                                    'flex',
                                                                                                justifyContent:
                                                                                                    'space-around',
                                                                                            }}
                                                                                        >
                                                                                            <button
                                                                                                onClick={() =>
                                                                                                    handleUpdateAccept(
                                                                                                        appointment.appointment_id
                                                                                                    )
                                                                                                }
                                                                                                className="btn btn-custom"
                                                                                                style={{
                                                                                                    backgroundColor:
                                                                                                        '',
                                                                                                }}
                                                                                            >
                                                                                                Accept
                                                                                            </button>
                                                                                            <button
                                                                                                onClick={() =>
                                                                                                    handleUpdateReject(
                                                                                                        appointment.appointment_id
                                                                                                    )
                                                                                                }
                                                                                                className="btn btn-custom"
                                                                                            >
                                                                                                Reject
                                                                                            </button>
                                                                                        </span>
                                                                                    </td>
                                                                                </tr>
                                                                            );
                                                                        }
                                                                    )}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        ) : (
                                            <div>
                                                No appointment request pending!
                                            </div>
                                        )}
                                    </div>
                                </div>
                                <div className="mt-3 row">
                                    <div className="mx-auto">
                                        <div className="container">
                                            <h3
                                                style={{
                                                    textAlign: 'center',
                                                    color: '#F4EBD0',
                                                }}
                                            >
                                                Upcoming Workouts
                                            </h3>
                                            <div className="row p-4">
                                                <table className="table table-dark table-striped">
                                                    <thead className="thead-dark">
                                                        <tr
                                                            className="text-center"
                                                            style={{
                                                                backgroundColor:
                                                                    '#181818',
                                                                color: '#F4EBD0',
                                                            }}
                                                        >
                                                            <th>Date</th>
                                                            <th>Time</th>
                                                            <th>Client Name</th>
                                                            <th>Comments</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {trainerAppointmentSuccess &&
                                                            trainerAppointments.appointments
                                                                .filter(
                                                                    (
                                                                        appointment
                                                                    ) =>
                                                                        appointment.status ===
                                                                            'accepted' &&
                                                                        new Date(
                                                                            `${appointment.date}T${appointment.time}`
                                                                        ) >=
                                                                            new Date()
                                                                )
                                                                .map(
                                                                    (
                                                                        appointment
                                                                    ) => {
                                                                        const matchingMember =
                                                                            members &&
                                                                            members.members
                                                                                ? members.members.find(
                                                                                      (
                                                                                          member
                                                                                      ) =>
                                                                                          member.id ===
                                                                                          appointment.member_id
                                                                                  )
                                                                                : '';
                                                                        const memberName =
                                                                            matchingMember
                                                                                ? `${matchingMember.first_name} ${matchingMember.last_name}`
                                                                                : 'Member not found.';

                                                                        const appointmentDateTime =
                                                                            new Date(
                                                                                `${appointment.date}T${appointment.time}`
                                                                            );

                                                                        const formatDate =
                                                                            new Date(
                                                                                appointment.date.replace(
                                                                                    /-/g,
                                                                                    '/'
                                                                                )
                                                                            );

                                                                        const formattedDate =
                                                                            formatDate.toLocaleDateString(
                                                                                'en-US',
                                                                                {
                                                                                    year: 'numeric',
                                                                                    month: 'short',
                                                                                    day: 'numeric',
                                                                                }
                                                                            );

                                                                        const formattedTime =
                                                                            appointmentDateTime.toLocaleTimeString(
                                                                                [],
                                                                                {
                                                                                    hour: '2-digit',
                                                                                    minute: '2-digit',
                                                                                    hour12: true,
                                                                                }
                                                                            );

                                                                        const endTime =
                                                                            new Date(
                                                                                appointmentDateTime.getTime()
                                                                            );
                                                                        endTime.setHours(
                                                                            endTime.getHours() +
                                                                                1
                                                                        );
                                                                        const formattedEndTime =
                                                                            endTime.toLocaleTimeString(
                                                                                [],
                                                                                {
                                                                                    hour: '2-digit',
                                                                                    minute: '2-digit',
                                                                                    hour12: true,
                                                                                }
                                                                            );
                                                                        const timeRange = `${formattedTime} - ${formattedEndTime}`;

                                                                        return (
                                                                            <tr
                                                                                className="text-center"
                                                                                key={
                                                                                    appointment.appointment_id
                                                                                }
                                                                            >
                                                                                <td>
                                                                                    {
                                                                                        formattedDate
                                                                                    }
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        timeRange
                                                                                    }
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        memberName
                                                                                    }
                                                                                </td>
                                                                                <td>
                                                                                    {
                                                                                        appointment.comments
                                                                                    }
                                                                                </td>
                                                                            </tr>
                                                                        );
                                                                    }
                                                                )}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
