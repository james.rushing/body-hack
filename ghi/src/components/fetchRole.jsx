export default async function FetchRole() {
    const API_URL = import.meta.env.VITE_API_HOST;
    const tokenApi = `${API_URL}/token`;
    const fetchConfig = {
        credentials: 'include',
    };

    const response = await fetch(tokenApi, fetchConfig);

    if (response.ok) {
        const data = await response.json();

        if (data !== null) {
            if (data.account['email'].includes('@bodyhack')) {
                const trainer_id = data.account['id'];
                const roleApi = `${API_URL}/trainers/${trainer_id}`;
                const roleResponse = await fetch(roleApi);

                if (roleResponse.ok) {
                    const roleData = await roleResponse.json();
                    return roleData;
                }
            } else {
                const member_id = data.account['id'];
                const roleApi = `${API_URL}/members/${member_id}`;
                const roleResponse = await fetch(roleApi);
                if (roleResponse.ok) {
                    const roleData = await roleResponse.json();
                    return roleData;
                } else {
                    console.error('Failed to get role data');
                }
            }
        } else {
            console.error('Failed to fetch token');
        }
    }
    if (data === null) {
        console.error('fetchRole: Failed to get id');
    }
}
