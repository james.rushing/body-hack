import { useState, useEffect } from 'react';
import '../index.css';

const BackgroundCycle = () => {
    const backgroundImages = ['bgimage1.jpg', 'bgimage2.jpg', 'bgimage3.jpg'];

    const [currentIndex, setCurrentIndex] = useState(0);

    useEffect(() => {
        const interval = setInterval(() => {
            setCurrentIndex(
                (prevIndex) => (prevIndex + 1) % backgroundImages.length
            );
        }, 10000);
        return () => clearInterval(interval);
    }, []);

    const backgroundStyle = {
        backgroundImage: `url(${backgroundImages[currentIndex]})`,
    };

    return <div id="background-container" style={backgroundStyle}></div>;
};

export default BackgroundCycle;
