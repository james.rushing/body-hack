import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Select from 'react-select';
import ErrorModal from './ErrorModal';
import { selectId } from '../../slices/userSlice';

const categoryList = [
    { value: 'abs', label: 'Abs' },
    { value: 'arms', label: 'Arms' },
    { value: 'back', label: 'Back' },
    { value: 'cardio', label: 'Cardio' },
    { value: 'chest', label: 'Chest' },
    { value: 'legs', label: 'Legs' },
    { value: 'shoulders', label: 'Shoulders' },
];

function CreatePersonalWorkoutForm() {
    const API_URL = import.meta.env.VITE_API_HOST;
    const userId = useSelector(selectId);
    const [errorModal, setErrorModal] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [trainers, setTrainers] = useState([]);
    const [workoutFormData, setWorkoutFormData] = useState({
        category: '',
        personal_or_trainer: '',
        trainer: null,
        date: '',
        start_time: '',
        end_time: '',
        member_id: userId,
    });
    const [circuitFormDataList, setCircuitFormDataList] = useState([
        {
            exercise: '',
            weight: '',
            reps_or_time: '',
            workout_id: 0,
            sets: 0,
        },
    ]);
    const [categoryOptions, setCategoryOptions] = useState([]);

    const getTrainers = async () => {
        const url = `${API_URL}/trainers`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTrainers(data.trainers);
        }
    };

    useEffect(() => {
        getTrainers();
    }, []);

    const handleWorkoutChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setWorkoutFormData({
            ...workoutFormData,
            [inputName]: value,
        });
    };

    const handleCategoryChange = (selectedOptions) => {
        let arr = Array.from(selectedOptions, (option) => option.value);
        setWorkoutFormData({
            ...workoutFormData,
            ['category']: arr.join(','),
        });

        setCategoryOptions(selectedOptions);
    };

    const handleCircuitChange = (i) => (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        circuitFormDataList[i][inputName] = value;
        setCircuitFormDataList([...circuitFormDataList]);
    };

    const addCircuit = (i) => (e) => {
        e.preventDefault();

        setCircuitFormDataList([
            ...circuitFormDataList.slice(0, i + 1),
            {
                exercise: '',
                weight: '',
                reps_or_time: '',
                workout_id: 0,
                sets: 0,
            },
            ...circuitFormDataList.slice(i + 1),
        ]);
    };

    const removeCircuit = (i) => (e) => {
        e.preventDefault();

        circuitFormDataList.splice(i, 1);
        setCircuitFormDataList([...circuitFormDataList]);
    };

    const handleCloseModal = () => {
        setErrorModal(false);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const workoutsURL = `${API_URL}/workouts/mine`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(workoutFormData),
            headers: {
                'Content-Type': 'application/json',
            },
            credentials: 'include',
        };

        const response = await fetch(workoutsURL, fetchConfig);

        if (response.ok) {
            const new_workout = await response.json();
            const workout_id = new_workout.id;
            console.log(workout_id);

            const circuitURL = `${API_URL}/workouts/mine/${workout_id}/circuits`;

            let ok = true;
            for (let circuit of circuitFormDataList) {
                circuit.workout_id = workout_id;

                const fetchConfig = {
                    method: 'post',
                    body: JSON.stringify(circuit),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    credentials: 'include',
                };

                const response = await fetch(circuitURL, fetchConfig);
                if (!response.ok) {
                    ok = false;
                    setErrorModal(true);
                    setErrorMessage(
                        'Circuit can not be added, please try again!'
                    );
                }
            }

            if (ok) {
                setWorkoutFormData({
                    ...workoutFormData,
                    category: '',
                    personal_or_trainer: '',
                    trainer: null,
                    date: '',
                    start_time: '',
                    end_time: '',
                });
                setCategoryOptions([]);

                setCircuitFormDataList([
                    {
                        exercise: '',
                        weight: '',
                        reps_or_time: '',
                        workout_id: 0,
                        workout_id: 0,
                        sets: 0,
                    },
                ]);
            }
        } else {
            setErrorModal(true);
            setErrorMessage('Workout can not be made, please try again!');
        }
    };

    return (
        <div className="member-background-container">
            <div className="content-container container-fluid mt-5">
                <div className="container col-md-7">
                    <div className="card-body col-md-12">
                        <h1
                            className="fw-bold profile-text"
                            style={{ textAlign: 'center' }}
                        >
                            Add a New Workout Session
                        </h1>
                        <div className="row">
                            {errorModal && (
                                <ErrorModal
                                    isOpen={true}
                                    message={errorMessage}
                                    onClose={handleCloseModal}
                                />
                            )}
                            <div className="col">
                                <div>
                                    <div>
                                        <form
                                            onSubmit={handleSubmit}
                                            id="create-workout-form"
                                        >
                                            <div className="mb-3">
                                                <label
                                                    htmlFor="personal_or_trainer"
                                                    className="fw-bold profile-text"
                                                >
                                                    Personal or With Trainer:
                                                </label>
                                                <select
                                                    style={{
                                                        marginLeft: '10px',
                                                    }}
                                                    onChange={
                                                        handleWorkoutChange
                                                    }
                                                    name="personal_or_trainer"
                                                    id="personal_or_trainer"
                                                    required
                                                    className="custom-input"
                                                >
                                                    <option value="">
                                                        Select Personal or
                                                        Trainer
                                                    </option>
                                                    <option value="personal">
                                                        Personal
                                                    </option>
                                                    <option value="trainer">
                                                        Trainer
                                                    </option>
                                                </select>
                                            </div>

                                            <div className="mb-3">
                                                <label
                                                    htmlFor="trainer"
                                                    className="fw-bold profile-text"
                                                >
                                                    {' '}
                                                    If trainer:
                                                </label>
                                                <select
                                                    style={{
                                                        marginLeft: '10px',
                                                    }}
                                                    onChange={
                                                        handleWorkoutChange
                                                    }
                                                    name="trainer"
                                                    id="trainer"
                                                    required={
                                                        workoutFormData.personal_or_trainer ===
                                                        'trainer'
                                                    }
                                                    className="custom-input"
                                                >
                                                    <option value="">
                                                        Select a Trainer
                                                    </option>
                                                    {trainers.map((trainer) => {
                                                        return (
                                                            <option
                                                                key={trainer.id}
                                                                value={
                                                                    trainer.id
                                                                }
                                                            >
                                                                {
                                                                    trainer.first_name
                                                                }{' '}
                                                                {
                                                                    trainer.last_name
                                                                }
                                                            </option>
                                                        );
                                                    })}
                                                </select>
                                            </div>

                                            <div>
                                                <label
                                                    htmlFor="category"
                                                    className="fw-bold profile-text"
                                                >
                                                    Category:
                                                </label>
                                                <Select
                                                    defaultValue={[]}
                                                    isMulti
                                                    options={categoryList}
                                                    name="category"
                                                    id="category"
                                                    className="basic-multi-select custom-input text-dark mb-3"
                                                    classNamePrefix="select"
                                                    required
                                                    value={categoryOptions}
                                                    onChange={
                                                        handleCategoryChange
                                                    }
                                                />
                                            </div>

                                            <div className="form-floating mb-3">
                                                <input
                                                    value={workoutFormData.date}
                                                    onChange={
                                                        handleWorkoutChange
                                                    }
                                                    placeholder="date"
                                                    required
                                                    type="date"
                                                    name="date"
                                                    id="date"
                                                    className="form-control custom-input"
                                                />
                                                <label
                                                    htmlFor="date"
                                                    className="text-dark"
                                                >
                                                    Date
                                                </label>
                                            </div>

                                            <div className="form-floating mb-3">
                                                <input
                                                    value={
                                                        workoutFormData.start_time
                                                    }
                                                    onChange={
                                                        handleWorkoutChange
                                                    }
                                                    placeholder="start_time"
                                                    required
                                                    type="time"
                                                    name="start_time"
                                                    id="start_time"
                                                    className="form-control custom-input"
                                                />
                                                <label
                                                    htmlFor="start_time"
                                                    className="text-dark"
                                                >
                                                    Start Time
                                                </label>
                                            </div>

                                            <div className="form-floating mb-3">
                                                <input
                                                    value={
                                                        workoutFormData.end_time
                                                    }
                                                    onChange={
                                                        handleWorkoutChange
                                                    }
                                                    placeholder="end_time"
                                                    required
                                                    type="time"
                                                    name="end_time"
                                                    id="end_time"
                                                    className="form-control custom-input"
                                                />
                                                <label
                                                    htmlFor="end_time"
                                                    className="text-dark"
                                                >
                                                    End Time
                                                </label>
                                            </div>

                                            <table
                                                className="table table-dark table-striped"
                                                id="circuits"
                                            >
                                                <thead>
                                                    <tr
                                                        style={{
                                                            color: '#F4EBD0',
                                                        }}
                                                    >
                                                        <th className="text-center">
                                                            Exercise
                                                        </th>
                                                        <th className="text-center">
                                                            Weight
                                                        </th>
                                                        <th className="text-center">
                                                            Reps/Time
                                                        </th>
                                                        <th className="text-center">
                                                            Sets
                                                        </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {circuitFormDataList.map(
                                                        (circuit, i) => {
                                                            return (
                                                                <tr
                                                                    id={i}
                                                                    key={i}
                                                                >
                                                                    <td>
                                                                        <input
                                                                            type="text"
                                                                            name="exercise"
                                                                            value={
                                                                                circuit.exercise
                                                                            }
                                                                            onChange={handleCircuitChange(
                                                                                i
                                                                            )}
                                                                            className="form-control"
                                                                        />
                                                                    </td>
                                                                    <td>
                                                                        <input
                                                                            type="text"
                                                                            name="weight"
                                                                            value={
                                                                                circuit.weight
                                                                            }
                                                                            onChange={handleCircuitChange(
                                                                                i
                                                                            )}
                                                                            className="form-control"
                                                                        />
                                                                    </td>
                                                                    <td>
                                                                        <input
                                                                            type="text"
                                                                            name="reps_or_time"
                                                                            value={
                                                                                circuit.reps_or_time
                                                                            }
                                                                            onChange={handleCircuitChange(
                                                                                i
                                                                            )}
                                                                            className="form-control"
                                                                        />
                                                                    </td>
                                                                    <td>
                                                                        <input
                                                                            type="number"
                                                                            name="sets"
                                                                            value={
                                                                                circuit.sets
                                                                            }
                                                                            onChange={handleCircuitChange(
                                                                                i
                                                                            )}
                                                                            className="form-control"
                                                                        />
                                                                    </td>
                                                                    <td>
                                                                        <button
                                                                            className="btn btn-custom"
                                                                            style={{
                                                                                fontSize:
                                                                                    '1.5em',
                                                                                paddingTop:
                                                                                    '0',
                                                                                fontWeight:
                                                                                    'bold',
                                                                            }}
                                                                            onClick={addCircuit(
                                                                                i
                                                                            )}
                                                                        >
                                                                            +
                                                                        </button>
                                                                    </td>
                                                                    <td>
                                                                        {i >
                                                                            0 && (
                                                                            <button
                                                                                className="btn btn-custom"
                                                                                style={{
                                                                                    fontSize:
                                                                                        '1.5em',
                                                                                    paddingTop:
                                                                                        '0',
                                                                                    fontWeight:
                                                                                        'bold',
                                                                                }}
                                                                                onClick={removeCircuit(
                                                                                    i
                                                                                )}
                                                                            >
                                                                                -
                                                                            </button>
                                                                        )}
                                                                    </td>
                                                                </tr>
                                                            );
                                                        }
                                                    )}
                                                </tbody>
                                            </table>

                                            <button className="btn btn-custom btn-custom-submit">
                                                Submit
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CreatePersonalWorkoutForm;
