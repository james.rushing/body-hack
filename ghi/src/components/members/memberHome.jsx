import { Link, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useGetMemberAppointmentsQuery } from '../../services/appointments';
import { useGetTrainersQuery } from '../../services/gettrainers';
import { useGetMyWorkoutsQuery } from '../../services/personalWorkoutsApi';
import Model from 'react-body-highlighter';
import { selectRole, selectFullName } from '../../slices/userSlice';

export default function MemberHome() {
    const userRole = useSelector(selectRole);
    const fullName = useSelector(selectFullName);
    const [bodymapData, setBodymapData] = useState({});
    const [showModel1, setShowModel1] = useState(true);
    const [showModel2, setShowModel2] = useState(false);
    const [lastSevenWorkouts, setLastSevenWorkouts] = useState([]);
    const [nextSevenAppts, setNextSevenAppts] = useState([]);
    const navigate = useNavigate();

    const {
        data: memberAppointments = [],
        isLoading: memberAppointmentLoading,
        isSuccess: memberAppointmentSuccess,
        isError: memberAppointmentError,
        refetch: refetchAppointments,
    } = useGetMemberAppointmentsQuery();

    const { data: trainers = [], isSuccess: trainersSuccess } =
        useGetTrainersQuery();

    const {
        data: myWorkouts = [],
        isSuccess: myWorkoutsSuccess,
        isError: myWorkoutsError,
        refetch: refetchWorkouts,
    } = useGetMyWorkoutsQuery();

    const muscleGroupMapping = {
        abs: ['abs', 'obliques'],
        arms: ['triceps', 'biceps'],
        back: ['upper-back', 'lower-back'],
        legs: ['quadriceps', 'hamstring', 'gluteal', 'adductor', 'abductors'],
        shoulders: ['front-deltoids', 'back-deltoids'],
    };

    function getStatusColor(status) {
        switch (status) {
            case 'rejected':
                return '#ffb09c';
            case 'accepted':
                return '#97bc82';
            case 'pending':
                return '#FCF4A3';
            default:
                return 'gray';
        }
    }

    // useEffect(() => {
    //     if (memberApptError && memberApptError.status === 401) {
    //        handleLogout();
    //     }
    // }, [memberApptError]);

    useEffect(() => {
        refetchWorkouts();
        refetchAppointments();
    }, []);

    // This gets the workouts from the last seven days and
    // generates the bodymap data from them.

    useEffect(() => {
        if (myWorkouts && myWorkoutsSuccess) {
            try {
                const today = new Date();
                const sevenDaysAgo = new Date(today);
                sevenDaysAgo.setDate(sevenDaysAgo.getDate() - 7);

                const lastSevenDaysWorkouts =
                    myWorkouts.personal_workouts.filter((workout) => {
                        const workoutDate = new Date(workout.date);
                        return (
                            workoutDate >= sevenDaysAgo && workoutDate <= today
                        );
                    });
                setLastSevenWorkouts(lastSevenDaysWorkouts);

                const categoriesAndDates = lastSevenDaysWorkouts.map(
                    (workout) => ({
                        category: workout.category.split(','),
                        date: workout.date,
                    })
                );

                const categoryFrequency = {};

                categoriesAndDates.forEach((entry) => {
                    entry.category.forEach((category) => {
                        categoryFrequency[category] =
                            (categoryFrequency[category] || 0) + 1;
                    });
                });
                const bodyMap = Object.entries(categoryFrequency).map(
                    ([category, frequency]) => ({
                        name: category,
                        muscles: muscleGroupMapping[category] || [],
                        frequency: frequency,
                    })
                );
                setBodymapData(bodyMap);
            } catch (e) {
                console.log('Error fetching bodymap data.');
            }
        } else if (myWorkoutsError) {
            console.log('Error fetching workouts.');
        }
    }, [myWorkoutsSuccess, myWorkouts]);

    // This controls the 5-second rotation of front and back view of the bodymap.

    useEffect(() => {
        const interval = setInterval(() => {
            setShowModel1(false);
            setShowModel2(true);
            setTimeout(() => {
                setShowModel1(true);
                setShowModel2(false);
            }, 5000);
        }, 10000);
        return () => clearInterval(interval);
    }, []);

    // This ensures that you have a saved role (and therefore have logged in).

    useEffect(() => {
        if (!userRole) {
            navigate('/');
        }
    }, [userRole, navigate]);

    // This gets the last seven days' appointments.

    useEffect(() => {
        if (memberAppointments && memberAppointmentSuccess) {
            try {
                const today = new Date();
                const sevenDaysAhead = new Date(today);
                sevenDaysAhead.setDate(sevenDaysAhead.getDate() + 7);

                const nextSevenDaysAppts =
                    memberAppointments.appointments.filter((appointment) => {
                        const apptDate = new Date(appointment.date);
                        return apptDate <= sevenDaysAhead && apptDate >= today;
                    });
                setNextSevenAppts(nextSevenDaysAppts);
            } catch (e) {
                console.log('Error fetching last seven appointments.');
            }
        }
    }, [memberAppointments, memberAppointmentSuccess]);

    return (
        <div className="member-background-container">
            <div className="container-fluid content-container">
                <div className="row mt-3">
                    <div className="col-md-12">
                        <h1
                            style={{
                                textAlign: 'center',
                                color: '#290202',
                                textShadow: '1px 1px 3px #c0a25e',
                                color: '#F4EBD0',
                            }}
                        >
                            Welcome {fullName}
                        </h1>
                    </div>
                </div>
                <div className="row mt-5 justify-content-center">
                    <div className="col-md-8">
                        {myWorkouts && myWorkoutsSuccess ? (
                            <div className="container">
                                <div className="row">
                                    <h3
                                        style={{
                                            textAlign: 'center',
                                            color: '#290202',
                                            textShadow: '1px 1px 3px #c0a25e',
                                            color: '#F4EBD0',
                                        }}
                                    >
                                        Last Week's Workouts
                                    </h3>
                                </div>

                                <div className="row p-4">
                                    <div className="col-md-7">
                                        <table className="table table-striped">
                                            <thead className="">
                                                <tr
                                                    className="text-center"
                                                    style={{
                                                        backgroundColor:
                                                            ' #181818',
                                                        color: '#F4EBD0',
                                                    }}
                                                >
                                                    <th>Date</th>
                                                    <th>Category</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {lastSevenWorkouts.length >
                                                0 ? (
                                                    lastSevenWorkouts.map(
                                                        (workout) => {
                                                            const formatDate =
                                                                new Date(
                                                                    workout.date.replace(
                                                                        /-/g,
                                                                        '/'
                                                                    )
                                                                );

                                                            const formattedDate =
                                                                formatDate.toLocaleDateString(
                                                                    'en-US',
                                                                    {
                                                                        year: 'numeric',
                                                                        month: 'short',
                                                                        day: 'numeric',
                                                                    }
                                                                );

                                                            return (
                                                                <tr
                                                                    key={
                                                                        workout.id
                                                                    }
                                                                    style={{
                                                                        backgroundColor:
                                                                            'white',
                                                                        textAlign:
                                                                            'center',
                                                                    }}
                                                                >
                                                                    <td>
                                                                        {
                                                                            formattedDate
                                                                        }
                                                                    </td>
                                                                    <td>
                                                                        {
                                                                            workout.category
                                                                        }
                                                                    </td>
                                                                </tr>
                                                            );
                                                        }
                                                    )
                                                ) : (
                                                    <tr>
                                                        <td
                                                            colSpan="2"
                                                            style={{
                                                                textAlign:
                                                                    'center',
                                                                backgroundColor:
                                                                    'gray',
                                                            }}
                                                        >
                                                            No workouts in the
                                                            last 7 days.{' '}
                                                        </td>
                                                    </tr>
                                                )}
                                            </tbody>
                                        </table>
                                    </div>

                                    <div className="col-md-5 justify-content-center align-items-center">
                                        <div className="body-box mb-2">
                                            <div className="models-container">
                                                {lastSevenWorkouts.length > 0 &&
                                                bodymapData &&
                                                bodymapData.length > 0 ? (
                                                    <>
                                                        {showModel1 && (
                                                            <div
                                                                className={`model ${
                                                                    showModel1
                                                                        ? ''
                                                                        : 'hidden'
                                                                }`}
                                                            >
                                                                <Model
                                                                    style={{
                                                                        height: '300px',
                                                                        marginTop:
                                                                            '20px',
                                                                        marginLeft:
                                                                            '-40px',
                                                                    }}
                                                                    data={
                                                                        bodymapData
                                                                    }
                                                                    highlightedColors={[
                                                                        '#fee12b',
                                                                        '#ff7417',
                                                                        '#ff2800',
                                                                    ]}
                                                                    bodyColor="#808080"
                                                                />
                                                            </div>
                                                        )}
                                                        {showModel2 && (
                                                            <div
                                                                className={`model ${
                                                                    showModel2
                                                                        ? ''
                                                                        : 'hidden'
                                                                }`}
                                                            >
                                                                <Model
                                                                    style={{
                                                                        height: '300px',
                                                                        marginTop:
                                                                            '20px',
                                                                        marginLeft:
                                                                            '-40px',
                                                                    }}
                                                                    type="posterior"
                                                                    data={
                                                                        bodymapData
                                                                    }
                                                                    highlightedColors={[
                                                                        '#fee12b',
                                                                        '#ff7417',
                                                                        '#ff2800',
                                                                    ]}
                                                                    bodyColor="#808080"
                                                                />
                                                            </div>
                                                        )}
                                                    </>
                                                ) : (
                                                    <Model
                                                        style={{
                                                            height: '300px',
                                                            marginTop: '20px',
                                                            marginLeft: '-40px',
                                                        }}
                                                        highlightedColors={[
                                                            '#fee12b',
                                                            '#ff7417',
                                                            '#ff2800',
                                                        ]}
                                                        bodyColor="#808080"
                                                    />
                                                )}
                                            </div>
                                        </div>
                                        <div className="container px-3">
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    marginBottom: '5px',
                                                }}
                                            >
                                                <div
                                                    style={{
                                                        width: '20px',
                                                        height: '20px',
                                                        backgroundColor:
                                                            '#ff2800',
                                                        marginRight: '10px',
                                                        borderRadius: '50%',
                                                        border: '1px solid silver',
                                                    }}
                                                ></div>
                                                <span
                                                    style={{
                                                        fontSize: '0.8em',
                                                    }}
                                                >
                                                    3+ Times
                                                </span>
                                            </div>
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    marginBottom: '5px',
                                                }}
                                            >
                                                <div
                                                    style={{
                                                        width: '20px',
                                                        height: '20px',
                                                        backgroundColor:
                                                            '#ff7417',
                                                        marginRight: '10px',
                                                        borderRadius: '50%',
                                                        border: '1px solid silver',
                                                    }}
                                                ></div>
                                                <span
                                                    style={{
                                                        fontSize: '0.8em',
                                                    }}
                                                >
                                                    2 Times
                                                </span>
                                            </div>
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    marginBottom: '5px',
                                                }}
                                            >
                                                <div
                                                    style={{
                                                        width: '20px',
                                                        height: '20px',
                                                        backgroundColor:
                                                            '#fee12b',
                                                        marginRight: '10px',
                                                        borderRadius: '50%',
                                                        border: '1px solid silver',
                                                    }}
                                                ></div>
                                                <span
                                                    style={{
                                                        fontSize: '0.8em',
                                                    }}
                                                >
                                                    1 Time
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            <div className="container">
                                <div className="row py-2">
                                    <h5
                                        style={{
                                            textAlign: 'center',
                                            color: '#F4EBD0',
                                        }}
                                    >
                                        No workouts available. Would you like to{' '}
                                        <Link to="/workouts/create">
                                            <button className="btn btn-custom">
                                                Add a Workout
                                            </button>
                                        </Link>{' '}
                                        ?
                                    </h5>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
                <div className="row mt-5 justify-content-center">
                    <div className="col-md-8">
                        {memberAppointmentLoading && <div>Loading...</div>}
                        {memberAppointmentError && (
                            <div>Error fetching appointments</div>
                        )}

                        {userRole && userRole === 'member' && (
                            <div>
                                {memberAppointmentSuccess && trainersSuccess ? (
                                    <div className="container">
                                        <div className="row">
                                            <h3
                                                style={{
                                                    textAlign: 'center',
                                                    color: '#F4EBD0',
                                                }}
                                            >
                                                Appointments
                                            </h3>
                                        </div>
                                        <div className="row p-4">
                                            <div className="col-md-7">
                                                <table className="table">
                                                    <thead className="">
                                                        <tr
                                                            className="text-center"
                                                            style={{
                                                                backgroundColor:
                                                                    '#181818',
                                                                color: '#F4EBD0',
                                                            }}
                                                        >
                                                            <th>Date</th>
                                                            <th>Time</th>
                                                            <th>Trainer</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {nextSevenAppts.map(
                                                            (appointment) => {
                                                                const matchingTrainer =
                                                                    trainers.trainers.find(
                                                                        (
                                                                            trainer
                                                                        ) =>
                                                                            trainer.id ===
                                                                            appointment.trainer_id
                                                                    );
                                                                const trainerName =
                                                                    matchingTrainer
                                                                        ? `${matchingTrainer.first_name} ${matchingTrainer.last_name}`
                                                                        : 'Trainer name not found.';

                                                                const formatDate =
                                                                    new Date(
                                                                        appointment.date.replace(
                                                                            /-/g,
                                                                            '/'
                                                                        )
                                                                    );

                                                                const appointmentDateTime =
                                                                    new Date(
                                                                        `${appointment.date}T${appointment.time}`
                                                                    );

                                                                const formattedDate =
                                                                    formatDate.toLocaleDateString(
                                                                        'en-US',
                                                                        {
                                                                            year: 'numeric',
                                                                            month: 'short',
                                                                            day: 'numeric',
                                                                        }
                                                                    );

                                                                const formattedTime =
                                                                    appointmentDateTime.toLocaleTimeString(
                                                                        [],
                                                                        {
                                                                            hour: '2-digit',
                                                                            minute: '2-digit',
                                                                            hour12: true,
                                                                        }
                                                                    );

                                                                const endTime =
                                                                    new Date(
                                                                        appointmentDateTime.getTime()
                                                                    );

                                                                endTime.setHours(
                                                                    endTime.getHours() +
                                                                        1
                                                                );

                                                                const formattedEndTime =
                                                                    endTime.toLocaleTimeString(
                                                                        [],
                                                                        {
                                                                            hour: '2-digit',
                                                                            minute: '2-digit',
                                                                            hour12: true,
                                                                        }
                                                                    );

                                                                const timeRange = `${formattedTime} - ${formattedEndTime}`;
                                                                return (
                                                                    <tr
                                                                        key={
                                                                            appointment.appointment_id
                                                                        }
                                                                        style={{
                                                                            backgroundColor:
                                                                                getStatusColor(
                                                                                    appointment.status
                                                                                ),
                                                                        }}
                                                                    >
                                                                        <td>
                                                                            {
                                                                                formattedDate
                                                                            }
                                                                        </td>
                                                                        <td>
                                                                            {
                                                                                timeRange
                                                                            }
                                                                        </td>
                                                                        <td>
                                                                            {
                                                                                trainerName
                                                                            }
                                                                        </td>
                                                                    </tr>
                                                                );
                                                            }
                                                        )}
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="col-md-5 mt-3 my-auto">
                                                <div className="d-flex justify-content-center">
                                                    <Link to="/appointments/create">
                                                        <button className="btn btn-custom btn-appointment rounded-circle">
                                                            Schedule Appointment
                                                        </button>
                                                    </Link>
                                                </div>
                                                <div className="container mt-5">
                                                    <div
                                                        style={{
                                                            display: 'flex',
                                                            marginBottom: '5px',
                                                            marginLeft: '10px',
                                                        }}
                                                    >
                                                        <div
                                                            style={{
                                                                width: '20px',
                                                                height: '20px',
                                                                backgroundColor:
                                                                    '#97bc82',
                                                                marginRight:
                                                                    '10px',
                                                                borderRadius:
                                                                    '5px',
                                                                border: '1px solid silver',
                                                            }}
                                                        ></div>
                                                        <span
                                                            style={{
                                                                fontSize:
                                                                    '0.8em',
                                                            }}
                                                        >
                                                            Accepted
                                                        </span>
                                                    </div>
                                                    <div
                                                        style={{
                                                            display: 'flex',
                                                            marginBottom: '5px',
                                                            marginLeft: '10px',
                                                        }}
                                                    >
                                                        <div
                                                            style={{
                                                                width: '20px',
                                                                height: '20px',
                                                                backgroundColor:
                                                                    '#FCF4A3',
                                                                marginRight:
                                                                    '10px',
                                                                borderRadius:
                                                                    '5px',
                                                                border: '1px solid silver',
                                                            }}
                                                        ></div>
                                                        <span
                                                            style={{
                                                                fontSize:
                                                                    '0.8em',
                                                            }}
                                                        >
                                                            Pending
                                                        </span>
                                                    </div>
                                                    <div
                                                        style={{
                                                            display: 'flex',
                                                            marginBottom: '5px',
                                                            marginLeft: '10px',
                                                        }}
                                                    >
                                                        <div
                                                            style={{
                                                                width: '20px',
                                                                height: '20px',
                                                                backgroundColor:
                                                                    '#ffb09c',
                                                                marginRight:
                                                                    '10px',
                                                                borderRadius:
                                                                    '5px',
                                                                border: '1px solid silver',
                                                            }}
                                                        ></div>
                                                        <span
                                                            style={{
                                                                fontSize:
                                                                    '0.8em',
                                                            }}
                                                        >
                                                            Rejected
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <div className="row p-4">
                                        <div className="col-md-7">
                                            <table className="table">
                                                <thead className="">
                                                    <tr
                                                        className="text-center"
                                                        style={{
                                                            backgroundColor:
                                                                '#181818',
                                                            color: '#F4EBD0',
                                                        }}
                                                    >
                                                        <th>Date</th>
                                                        <th>Time</th>
                                                        <th>Trainer</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td
                                                            colSpan="2"
                                                            style={{
                                                                textAlign:
                                                                    'center',
                                                                backgroundColor:
                                                                    'gray',
                                                            }}
                                                        >
                                                            No appointments
                                                            available.
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                </div>
                {userRole === '' && <p>You are not logged in.</p>}
            </div>
        </div>
    );
}
