import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { selectRole } from '../../slices/userSlice';
import {
    useGetMyWorkoutsQuery,
    useDeleteMyWorkoutMutation,
} from '../../services/personalWorkoutsApi';
import CircuitsModal from './CircuitsModal';
import Select from 'react-select';

const categoryList = [
    { value: 'abs', label: 'Abs', key: 'abs' },
    { value: 'arms', label: 'Arms', key: 'arms' },
    { value: 'back', label: 'Back', key: 'value' },
    { value: 'cardio', label: 'Cardio', key: 'cardio' },
    { value: 'chest', label: 'Chest', key: 'chest' },
    { value: 'legs', label: 'Legs', key: 'legs' },
    {
        value: 'shoulders',
        label: 'Shoulders',
        keys: 'shoulders',
    },
];

const monthList = [
    { value: 1, label: 'January' },
    { value: 2, label: 'February' },
    { value: 3, label: 'March' },
    { value: 4, label: 'April' },
    { value: 5, label: 'May' },
    { value: 6, label: 'June' },
    { value: 7, label: 'July' },
    { value: 8, label: 'August' },
    { value: 9, label: 'September' },
    { value: 10, label: 'October' },
    { value: 11, label: 'November' },
    { value: 12, label: 'December' },
];

const initialSessionTypeList = ['personal', 'trainer'];

function MyWorkoutsList() {
    const [categoryFilterList] = useState(categoryList);
    const [categoryFilter, setCategoryFilter] = useState([]);
    const [monthFilterList] = useState(monthList);
    const [monthFilter, setMonthFilter] = useState('');
    const [yearFilterList, setYearFilterList] = useState([]);
    const [yearFilter, setYearFilter] = useState('');
    const [sessionTypeFilterList, setSessionTypeFilterList] = useState(
        initialSessionTypeList
    );
    const [sessionTypeFilter, setSessionTypeFilter] = useState('');
    const userRole = useSelector(selectRole);
    const [circuitDetailModal, setCircuitDetailModal] = useState(false);
    const [selectedWorkoutId, setSelectedWorkoutId] = useState('');
    const [modalPosition, setModalPosition] = useState({ top: 0, left: 0 });

    const {
        data: myWorkouts = [],
        isSuccess: myWorkoutsSuccess,
        refetch,
    } = useGetMyWorkoutsQuery();
    const [deleteMyWorkout, { isLoading, error }] =
        useDeleteMyWorkoutMutation();

    const getYearFilter = (myWorkouts) => {
        if (myWorkoutsSuccess) {
            const years = myWorkouts.personal_workouts.map((record) =>
                new Date(record.date).getFullYear()
            );
            const setYear = [...new Set(years)];
            setYearFilterList(setYear);
        }
    };

    const handleClick = (workoutId) => {
        setCircuitDetailModal(true);
        setSelectedWorkoutId(workoutId);
    };

    useEffect(() => {
        refetch();
    }, []);

    useEffect(() => {
        if (selectedWorkoutId) {
            const selectedRow = document.querySelector('.selected-row');

            const selectedRowRect = selectedRow.getBoundingClientRect();
            const rowWidth = selectedRowRect.width;
            const modalWidth = 500;
            const top = selectedRowRect.bottom;
            const left = selectedRowRect.left + (rowWidth - modalWidth) / 2;

            setModalPosition({ top, left });
        }
    }, [selectedWorkoutId]);

    useEffect(() => {
        getYearFilter(myWorkouts);
    }, [myWorkouts]);

    const handleCategoryFilterChange = (selectedOptions) => {
        setCategoryFilter(selectedOptions);
    };

    const handleDeleteClick = async (id) => {
        try {
            await deleteMyWorkout(id);
        } catch (error) {
            console.error('Failed to delete: ', error);
        }
    };

    function handleMonthFilterChange(e) {
        setMonthFilter(e.target.value);
    }

    function handleYearFilterChange(e) {
        setYearFilter(e.target.value);
    }

    function handleSessionTypeFilterChange(e) {
        setSessionTypeFilter(e.target.value);
    }

    function handleClearFiltersClick() {
        setCategoryFilter([]);
        setMonthFilter('');
        setYearFilter('');
        setSessionTypeFilter('');
    }

    function getFilterValue() {
        return myWorkouts.personal_workouts
            .filter((workouts_session) => {
                return workouts_session.personal_or_trainer
                    .toLowerCase()
                    .includes(sessionTypeFilter);
            })
            .filter((workouts_year) => {
                return workouts_year.date.includes(yearFilter);
            })
            .filter((workouts_month) => {
                return workouts_month.date
                    .substring(5, 7)
                    .includes(monthFilter);
            })
            .filter((workouts_category) => {
                if (categoryFilter.length !== 0) {
                    const arr_category = workouts_category.category.split(',');
                    let filters = [];
                    for (let item of categoryFilter) {
                        filters.push(item.value);
                    }
                    return arr_category.some((cate) => filters.includes(cate));
                } else {
                    return true;
                }
            });
    }

    return (
        <div className="member-background-container">
            <div className="content-container container-fluid ">
                <div className="d-flex justify-content-center">
                    <div className="col-md-8">
                        <h1 className="text-center pt-3 pb-3 fw-bold profile-text">
                            Workouts Journal
                        </h1>
                        <div className="container mb-3">
                            <div className="row p-4">
                                <div className="col">
                                    <label
                                        htmlFor="categoryFilter"
                                        className="fw-bold profile-text"
                                    >
                                        Category
                                    </label>
                                    <Select
                                        defaultValue={[]}
                                        isMulti
                                        options={categoryFilterList}
                                        name="category"
                                        className="custom-input basic-multi-select text-dark "
                                        classNamePrefix="select"
                                        id="categoryFilter"
                                        value={categoryFilter}
                                        onChange={handleCategoryFilterChange}
                                    />
                                </div>
                                <div className="col">
                                    <label
                                        htmlFor="monthFilter"
                                        className="fw-bold profile-text"
                                    >
                                        Month
                                    </label>
                                    <select
                                        className="custom-input form-select form-select-lg "
                                        id="monthFilter"
                                        onChange={handleMonthFilterChange}
                                        value={monthFilter}
                                    >
                                        <option value="">Select...</option>
                                        {monthFilterList.map((month) => (
                                            <option
                                                value={month.value}
                                                key={month.label}
                                            >
                                                {month.label}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <label
                                        htmlFor="yearFilter"
                                        className="fw-bold profile-text"
                                    >
                                        Year
                                    </label>
                                    <select
                                        className="custom-input form-select form-select-lg "
                                        id="yearFilter"
                                        onChange={handleYearFilterChange}
                                        value={yearFilter}
                                    >
                                        <option value="">Select...</option>
                                        {yearFilterList.map((year) => (
                                            <option value={year} key={year}>
                                                {year}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="col">
                                    <label
                                        htmlFor="sessionTypeFilter"
                                        className="fw-bold profile-text"
                                    >
                                        Session
                                    </label>
                                    <select
                                        className="custom-input form-select form-select-lg "
                                        id="sessionTypeFilter"
                                        onChange={handleSessionTypeFilterChange}
                                        value={sessionTypeFilter}
                                    >
                                        <option value="">Select...</option>
                                        {sessionTypeFilterList.map(
                                            (trainer) => (
                                                <option
                                                    value={trainer}
                                                    key={trainer}
                                                >
                                                    {trainer}
                                                </option>
                                            )
                                        )}
                                    </select>
                                </div>
                            </div>
                            <div className="row">
                                <div className="d-flex justify-content-center">
                                    <button
                                        className="btn btn-custom"
                                        onClick={handleClearFiltersClick}
                                    >
                                        Clear Filters
                                    </button>
                                </div>
                            </div>
                            <div>
                                {userRole && userRole === 'member' && (
                                    <div className="row p-4">
                                        {myWorkoutsSuccess ? (
                                            <table className="table table-dark table-striped">
                                                <thead>
                                                    <tr
                                                        style={{
                                                            backgroundColor:
                                                                '#181818',
                                                            color: '#F4EBD0',
                                                        }}
                                                    >
                                                        <th>Category</th>
                                                        <th>Date</th>
                                                        <th>Time</th>
                                                        <th>Session</th>
                                                        <th className="text-center">
                                                            Circuits
                                                        </th>
                                                        <th className="text-center">
                                                            Actions
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {getFilterValue().map(
                                                        (model) => {
                                                            let formatDate =
                                                                new Date(
                                                                    model.date.replace(
                                                                        /-/g,
                                                                        '/'
                                                                    )
                                                                );
                                                            let date_time_start = `${model.date}T${model.start_time}`;
                                                            let date_time_end = `${model.date}T${model.end_time}`;

                                                            let date = new Date(
                                                                formatDate
                                                            ).toLocaleDateString(
                                                                'en-US',
                                                                {
                                                                    year: 'numeric',
                                                                    month: 'short',
                                                                    day: 'numeric',
                                                                }
                                                            );
                                                            let time_start =
                                                                new Date(
                                                                    date_time_start
                                                                ).toLocaleTimeString(
                                                                    [],
                                                                    {
                                                                        hour: '2-digit',
                                                                        minute: '2-digit',
                                                                        hour12: true,
                                                                    }
                                                                );
                                                            let time_end =
                                                                new Date(
                                                                    date_time_end
                                                                ).toLocaleTimeString(
                                                                    [],
                                                                    {
                                                                        hour: '2-digit',
                                                                        minute: '2-digit',
                                                                        hour12: true,
                                                                    }
                                                                );

                                                            return (
                                                                <tr
                                                                    key={
                                                                        model.id
                                                                    }
                                                                    className={
                                                                        selectedWorkoutId ===
                                                                        model.id
                                                                            ? 'selected-row'
                                                                            : ''
                                                                    }
                                                                >
                                                                    <td>
                                                                        {
                                                                            model.category
                                                                        }
                                                                    </td>
                                                                    <td>
                                                                        {date}
                                                                    </td>
                                                                    <td>
                                                                        {
                                                                            time_start
                                                                        }
                                                                        -
                                                                        {
                                                                            time_end
                                                                        }
                                                                    </td>
                                                                    <td>
                                                                        {
                                                                            model.personal_or_trainer
                                                                        }
                                                                    </td>
                                                                    <td className="shrink">
                                                                        <button
                                                                            key={
                                                                                model.id
                                                                            }
                                                                            className="btn btn-custom"
                                                                            onClick={() =>
                                                                                handleClick(
                                                                                    model.id
                                                                                )
                                                                            }
                                                                        >
                                                                            View
                                                                            Circuits
                                                                        </button>
                                                                    </td>
                                                                    <td className="shrink">
                                                                        <button
                                                                            className="btn btn-custom"
                                                                            onClick={() =>
                                                                                handleDeleteClick(
                                                                                    model.id
                                                                                )
                                                                            }
                                                                        >
                                                                            Delete
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                            );
                                                        }
                                                    )}
                                                </tbody>
                                            </table>
                                        ) : (
                                            <div>
                                                There are no records of past
                                                workouts.
                                            </div>
                                        )}
                                        {circuitDetailModal && (
                                            <CircuitsModal
                                                isOpen={true}
                                                onClose={() => {
                                                    setSelectedWorkoutId(null);
                                                    setCircuitDetailModal(
                                                        false
                                                    );
                                                }}
                                                workout_id={selectedWorkoutId}
                                                style={{
                                                    top: `${modalPosition.top}px`,
                                                    left: `${modalPosition.left}px`,
                                                }}
                                            />
                                        )}
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MyWorkoutsList;
