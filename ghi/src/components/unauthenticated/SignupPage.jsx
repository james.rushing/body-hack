import { useState, useEffect } from 'react';
import useToken, { useAuthContext } from '@galvanize-inc/jwtdown-for-react';
import { useNavigate } from 'react-router-dom';
import FetchRole from '../fetchRole';
import { useDispatch } from 'react-redux';

const SignupForm = () => {
    const API_URL = import.meta.env.VITE_API_HOST;
    const [confirmPassword, setConfirmPassword] = useState('');
    const [error, setError] = useState('');
    const { setToken, token } = useAuthContext();
    const { register } = useToken();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [regSuccessful, setRegSuccessful] = useState(false);

    const [formData, setFormData] = useState({
        email: '',
        password: '',
        first_name: '',
        last_name: '',
    });

    const handleConfirmPasswordChange = (e) =>
        setConfirmPassword(e.target.value);

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (formData.password !== confirmPassword) {
            setError('Password did not match. Please confirm your password');
            return;
        }

        const create_url = `${API_URL}/api/accounts`;
        const check_email_url = `${API_URL}/members`;

        const userData = {
            username: formData.email,
            ...formData,
        };
        const fetchConfig = {
            method: 'get',
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(check_email_url, fetchConfig);

        if (response.ok) {
            const get_existing_member = await response.json();
            const isEmailNotInUse =
                get_existing_member.members &&
                !get_existing_member.members.some(
                    (member) => member.email === formData.email
                );
            if (isEmailNotInUse) {
                try {
                    const loginToken = await register(userData, create_url);
                    setToken(loginToken);
                    setRegSuccessful(true);
                } catch (e) {
                    setError('An unexpected error occurred.');
                }
            } else {
                setError(
                    'Entered email is already registered in the system. Please log in or use a different email to sign up.'
                );
                setRegSuccessful(false);
            }
        }
    };

    useEffect(() => {
        if (regSuccessful === false) {
            return;
        }
        const fetchRoleAndNavigate = async () => {
            try {
                let result = await FetchRole();
                console.log('Result: ', result);
                let id = result['id'];
                let role = result['role'];
                let fullName = `${result['first_name']} ${result['last_name']}`;
                if (role === 'member') {
                    navigate('/member');
                } else {
                    navigate('/trainer');
                }
            } catch (error) {
                console.error(error);
            }
        };

        if (token && regSuccessful) {
            fetchRoleAndNavigate();
        }
    }, [token, dispatch, navigate, regSuccessful]);

    return (
        <div className="member-background-container">
            <div className="content-container">
                <div className="container login-container text-bg-dark my-5 col-3 p-2 align-items-center">
                    <h5 className="card-title">Signup</h5>
                    <div className="card-body">
                        <form onSubmit={handleSubmit}>
                            <div className="mb-3">
                                {error && (
                                    <p style={{ color: 'red' }}>{error}</p>
                                )}
                                <label className="form-label signup-label">
                                    First Name
                                </label>
                                <input
                                    name="first_name"
                                    type="text"
                                    className="form-control"
                                    onChange={handleFormChange}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label className="form-label signup-label">
                                    Last Name
                                </label>
                                <input
                                    name="last_name"
                                    type="text"
                                    className="form-control"
                                    onChange={handleFormChange}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label className="form-label signup-label">
                                    Email
                                </label>
                                <input
                                    name="email"
                                    type="text"
                                    className="form-control"
                                    onChange={handleFormChange}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label className="form-label signup-label">
                                    Password
                                </label>
                                <input
                                    name="password"
                                    type="password"
                                    className="form-control"
                                    onChange={handleFormChange}
                                    required
                                />
                            </div>
                            <div className="mb-3">
                                <label className="form-label signup-label">
                                    Confirm Password
                                </label>
                                <input
                                    name="confirmPassword"
                                    type="password"
                                    className="form-control"
                                    onChange={handleConfirmPasswordChange}
                                    required
                                />
                            </div>
                            <div>
                                <input
                                    className="btn btn-custom btn-custom-submit"
                                    type="submit"
                                    value="Submit"
                                />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SignupForm;
