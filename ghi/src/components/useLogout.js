import { useDispatch } from 'react-redux';
import { useLogoutMutation } from '../services/loginmutation';
import { useNavigate } from 'react-router-dom';
import { resetUser } from '../slices/userSlice';
import { getTokenApi } from '../services/gettoken';

export default function useLogout() {
    const dispatch = useDispatch();
    const [logout] = useLogoutMutation();
    const navigate = useNavigate();

    return async function LogoutFunction() {
        try {
            await logout();
            dispatch(resetUser());
            dispatch(getTokenApi.util.invalidateTags(['Token']));
            navigate('/');
        } catch (error) {
            console.error('Logout failed: ', error);
        }
    };
}
