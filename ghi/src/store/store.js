import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { getTokenApi } from '../services/gettoken';
import appointmentsReducer from '../slices/appointmentsSlice';
import { appointmentsApi } from '../services/appointments';
import trainersReducer from '../slices/trainerSlice';
import { trainersApi } from '../services/gettrainers';
import { membersApi } from '../services/getmembers';
import { personalWorkoutApi } from '../services/personalWorkoutsApi';
import membersReducer from '../slices/membersSlice';
import { setupListeners } from '@reduxjs/toolkit/query';
import { loginApi } from '../services/loginmutation';
import { circuitsApi } from '../services/circuits';
import userReducer from '../slices/userSlice';

const rootReducer = combineReducers({
    [getTokenApi.reducerPath]: getTokenApi.reducer,
    user: userReducer,
    appointments: appointmentsReducer,
    [appointmentsApi.reducerPath]: appointmentsApi.reducer,
    trainers: trainersReducer,
    [trainersApi.reducerPath]: trainersApi.reducer,
    members: membersReducer,
    [membersApi.reducerPath]: membersApi.reducer,
    [personalWorkoutApi.reducerPath]: personalWorkoutApi.reducer,
    [loginApi.reducerPath]: loginApi.reducer,
    [circuitsApi.reducerPath]: circuitsApi.reducer,

})

export const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleWare) =>
        getDefaultMiddleWare().concat(getTokenApi.middleware, appointmentsApi.middleware, circuitsApi.middleware, trainersApi.middleware, membersApi.middleware, loginApi.middleware, personalWorkoutApi.middleware),
});

setupListeners(store.dispatch);

export default store;
