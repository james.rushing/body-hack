import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const API_URL = import.meta.env.VITE_API_HOST;

export const roleApi = createApi({
    reducerPath: 'role',
    baseQuery: fetchBaseQuery({ baseUrl: `${API_URL}` }),
    endpoints: (builder) => ({
        getTrainerRole: builder.query({
            query: (trainerId) => `trainers/${trainerId}`,
        }),
        getMemberRole: builder.query({
            query: (memberId) => `members/${memberId}`,
        }),
        getToken: builder.query({
            query: () => 'token',
        }),
    }),
});

export const { useGetTrainerRoleQuery, useGetMemberRoleQuery, useGetTokenQuery } = roleApi;
