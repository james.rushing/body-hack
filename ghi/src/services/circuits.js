import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

console.table(import.meta.env);
const API_URL = import.meta.env.VITE_API_HOST;

export const circuitsApi = createApi({
    reducerPath: 'circuits',
    baseQuery: fetchBaseQuery({
        baseUrl: API_URL,
        credentials: 'include',
    }),
    tagTypes: ['Circuits'],
    endpoints: (builder) => ({
        getCircuits: builder.query({
            query: (workoutId) => `/workouts/mine/${workoutId}/circuits`,
            providesTags: ['Circuits'],
        }),
        getCircuit: builder.query({
            query: (circuitId) => `/workouts/mine/circuits/${circuitId}`,
        }),
        editCircuit: builder.mutation({
            query: ({ circuitId, circuitForm }) => ({
                url: `/workouts/mine/circuits/${circuitId}`,
                method: 'PUT',
                body: circuitForm,
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }),
            invalidatesTags: ["Circuits"],
        })
    }),
});

export const { useGetCircuitsQuery, useGetCircuitQuery, useEditCircuitMutation } = circuitsApi;
