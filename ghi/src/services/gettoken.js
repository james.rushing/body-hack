import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

console.table(import.meta.env);
const API_URL = import.meta.env.VITE_API_HOST;

export const getTokenApi = createApi({
    reducerPath: "getToken",
    baseQuery: fetchBaseQuery({
        baseUrl: API_URL,
        credentials: 'include',
    }),
    tagTypes: ["Token"],
    endpoints: (builder) => ({
        getToken: builder.query({
            query: () => "/token",
            providesTags: ["Token"],
        }),
    })
})

export const { useGetTokenQuery } = getTokenApi;
