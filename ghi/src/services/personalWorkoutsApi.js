import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const personalWorkoutApi = createApi({
    reducerPath: 'myWorkouts',
    baseQuery: fetchBaseQuery({
        baseUrl: import.meta.env.VITE_API_HOST,
        credentials: 'include',
    }),
    tagTypes: ['PersonalWorkouts'],
    endpoints: (builder) => ({
        getMyWorkouts: builder.query({
            query: () => '/workouts/mine',
            providesTags: ['PersonalWorkouts']
        }),
        deleteMyWorkout: builder.mutation({
            query: (workout_id) => ({
                url: `/workouts/mine/${workout_id}`,
                method: 'DELETE',
            }),
            invalidatesTags: ['PersonalWorkouts']
        })
    })
});

export const { useGetMyWorkoutsQuery, useDeleteMyWorkoutMutation } = personalWorkoutApi;
