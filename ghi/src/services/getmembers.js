import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

console.table(import.meta.env);
const API_URL = import.meta.env.VITE_API_HOST;

export const membersApi = createApi({
    reducerPath: "members",
    baseQuery: fetchBaseQuery({
        baseUrl: API_URL,
        credentials: 'include',
    }),
    endpoints: (builder) => ({
        getMembers: builder.query({
            query: () => "/members",
        }),
        getOneMember: builder.query({
            query: (member_id) => `/members/${member_id}`
        }),
        updateProfile: builder.mutation({
            query: (member_id) => ({
                url: `/members/${member_id}`,
                method: 'PUT',
                body: { "status": "accepted" },
            }),
            invalidatesTags: ["TrainerAppointments"],
        }),
    }),
});

export const { useGetMembersQuery, useGetOneMemberQuery } = membersApi;
