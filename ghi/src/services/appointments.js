import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
console.table(import.meta.env);

const API_URL = import.meta.env.VITE_API_HOST;

export const appointmentsApi = createApi({
    reducerPath: 'appointments',
    baseQuery: fetchBaseQuery({
        baseUrl: API_URL,
        credentials: 'include',
    }),
    tagTypes: ['MemberAppointments', 'TrainerAppointments'],
    endpoints: (builder) => ({
        getMemberAppointments: builder.query({
            query: () => "/members/mine/appointments",
            providesTags: ['MemberAppointments'],
        }),
        getTrainerAppointments: builder.query({
            query: () => "/trainers/mine/appointments",
            providesTags: ['TrainerAppointments'],
        }),
        getSingleAppointment: builder.query({
            query: (appointment_id) => `/members/mine/appointments/${appointment_id}`,
        }),
        updateAccept: builder.mutation({
            query: appointmentId => ({
                url: `/trainer/mine/appointments/${appointmentId}`,
                method: 'PUT',
                body: { "status": "accepted" },
            }),
            invalidatesTags: ["TrainerAppointments"],
        }),
        updateReject: builder.mutation({
            query: appointmentId => ({
                url: `/trainer/mine/appointments/${appointmentId}`,
                method: 'PUT',
                body: { "status": "rejected" },
            }),
            invalidatesTags: ["TrainerAppointments"],
        }),
        createAppointment: builder.mutation({
            query: (formData) => ({
                url: '/members/mine/appointments',
                method: 'POST',
                body: formData,
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }),
            invalidatesTags: ["MemberAppointments"],
        }),
    }),
});

export const {
    useGetMemberAppointmentsQuery,
    useGetTrainerAppointmentsQuery,
    useGetSingleAppointmentQuery,
    useUpdateAcceptMutation,
    useUpdateRejectMutation,
    useCreateAppointmentMutation,
} = appointmentsApi;
