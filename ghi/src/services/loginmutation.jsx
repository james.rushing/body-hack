import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

console.table(import.meta.env);

const API_URL = import.meta.env.VITE_API_HOST;

export const loginApi = createApi({
    reducerPath: 'login',
    baseQuery: fetchBaseQuery({
        baseUrl: API_URL,
        credentials: 'include',
    }),
    endpoints: (builder) => ({
        login: builder.mutation({
            query: (credentials) => ({
                url: '/token',
                method: 'POST',
                body: credentials,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                credentials: 'include',
            }),
        }),
        getToken: builder.query({
            query: () => ({
                url: '/token',
                credentials: 'include',
            }),
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            }),
        }),
    }),
});

export const { useLoginMutation, useGetTokenQuery, useLogoutMutation } =
    loginApi;
