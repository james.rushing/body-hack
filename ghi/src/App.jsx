import { AuthProvider } from '@galvanize-inc/jwtdown-for-react';
import { Routes, Route } from 'react-router-dom';
import './App.css';
import Nav from './nav';
import LoginForm from './components/login';
import MemberHome from './components/members/memberHome';
import TrainerHome from './components/trainers/trainerHome';
import Home from './components/home';
import SignupForm from './components/unauthenticated/SignupPage';
import Footer from './components/footer';
import MyWorkoutsList from './components/members/MyWorkoutsList';
import Profile from './components/members/Profile';
import CreateAppointmentForm from './components/members/CreateAppointmentForm';
import CreatePersonalWorkoutForm from './components/members/CreatePersonalWorkoutForm';

// All your environment variables in vite are in this object
console.table(import.meta.env);

// When using environment variables, you should do a check to see if
// they are defined or not and throw an appropriate error message
const VITE_API_HOST = import.meta.env.VITE_API_HOST;

if (!VITE_API_HOST) {
    throw new Error('VITE_API_HOST is not defined');
}

function App() {
    return (
        <AuthProvider baseUrl={VITE_API_HOST}>
            <Nav />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/login" element={<LoginForm />} />
                <Route path="/member" element={<MemberHome />} />
                <Route path="/workouts/mine" element={<MyWorkoutsList />} />
                <Route path="/profile" element={<Profile />} />
                <Route path="/signup" element={<SignupForm />} />
                <Route
                    path="/appointments/create"
                    element={<CreateAppointmentForm />}
                />
                <Route
                    path="/workouts/create"
                    element={<CreatePersonalWorkoutForm />}
                />
                <Route path="/trainer" element={<TrainerHome />} />
            </Routes>
            <Footer />
        </AuthProvider>
    );
}

// export { API_URL };
export default App;
