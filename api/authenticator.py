import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.accounts import (
    AccountRepo,
    AccountOutWithHashedPassword,
    AccountOut,
)


class IncorrectEmailOrPassword(ValueError):
    pass


class AccountAuthenticator(Authenticator):
    async def get_account_data(
        self,
        email: str,
        accounts: AccountRepo,
    ):
        return accounts.get(email)

    def get_account_getter(
        self,
        accounts: AccountRepo = Depends(),
    ):
        return accounts

    def get_hashed_password(self, account: AccountOutWithHashedPassword):
        try:
            if account is not None:
                return account.hashed_password
        except Exception:
            raise IncorrectEmailOrPassword("Incorrect email or password")

    def get_account_data_for_cookie(self, account: AccountOut):

        return account.email, AccountOut(**account.dict())


authenticator = AccountAuthenticator(os.environ["SIGNING_KEY"])
