from models import AccountOutWithHashedPassword, AccountOut
from queries.pool import pool


class DuplicateAccountError(ValueError):
    pass


class AccountRepo:
    def get(self, email: str):
        with pool.connection() as conn:
            try:
                with conn.cursor() as cursor:
                    cursor.execute(
                        """
                        SELECT * from members where email = %s
                        UNION
                        SELECT * from trainers where email = %s;
                        """,
                        (email, email),
                    )
                    record = None
                    fetch = cursor.fetchall()
                    if len(fetch) > 0:
                        for row in fetch:
                            record = {}
                            for i, column in enumerate(cursor.description):
                                record[column.name] = row[i]
                        return AccountOutWithHashedPassword(**record)
            except Exception as e:
                return ("Exception:", {e})

    def create_user(
        self, data, hashed_password
    ) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            try:
                with conn.cursor() as cursor:
                    params = [
                        data.email,
                        data.first_name,
                        data.last_name,
                        hashed_password,
                    ]
                    cursor.execute(
                        """
                        INSERT INTO members (
                            email,
                            first_name,
                            last_name,
                            hashed_password
                        )
                        VALUES (%s, %s, %s, %s)
                        RETURNING first_name, last_name, email, id, role
                        """,
                        params,
                    )
                    conn.commit()
                    account = cursor.fetchone()
                    record = None
                    if account is not None:
                        record = {}
                        for i, column in enumerate(cursor.description):
                            record[column.name] = account[i]
                        return AccountOut(**record)
            except Exception as e:
                if "duplicate" in str(e):
                    raise DuplicateAccountError
                else:
                    raise

    def create_trainer(
            self, data, hashed_password
    ) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            try:
                with conn.cursor() as cursor:
                    params = [
                        data.email,
                        data.first_name,
                        data.last_name,
                        hashed_password,
                    ]
                    cursor.execute(
                        """
                        INSERT INTO trainers (
                            email,
                            first_name,
                            last_name,
                            hashed_password
                        )
                        VALUES (%s, %s, %s, %s)
                        RETURNING first_name, last_name, email, id
                        """,
                        params,
                    )
                    conn.commit()
                    account = cursor.fetchone()
                    record = None
                    if account is not None:
                        record = {}
                        for i, column in enumerate(cursor.description):
                            record[column.name] = account[i]
                        return AccountOut(**record)
            except Exception as e:
                if "duplicate" in str(e):
                    raise DuplicateAccountError
                else:
                    raise
