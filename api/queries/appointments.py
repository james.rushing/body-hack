from queries.pool import pool
from typing import List, Union, Optional
from fastapi import HTTPException, status
from models import (
    AppointmentIn,
    AppointmentOut,
    AppointmentStatusIn,
    Error,
)


class AppointmentRepository:
    def member_get_all(
        self,
        member_id: int,
    ) -> Union[Error, List[AppointmentOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT appt.appointment_id
                            , appt.date
                            , appt.time
                            , appt.comments
                            , appthub.trainer_id
                            , appthub.member_id
                            , appt.status
                        FROM appointments appt
                        LEFT JOIN appointmentshub appthub
                            ON appt.appointment_id = appthub.appointment_id
                        WHERE member_id = %s
                        ORDER BY date;
                        """,
                        [member_id],
                    )
                    result = []
                    if db:
                        for record in db:
                            one_appointment = self.record_to_appointment_out(
                                record
                            )
                            result.append(one_appointment)
                        return result
                    else:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="No appointments found.",
                        )
        except Exception:
            return {"message": "Failed to load your appointments."}

    def trainer_get_all(
        self, trainer_id: int
    ) -> Union[Error, List[AppointmentOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT appt.appointment_id
                            , appt.date
                            , appt.time
                            , appt.comments
                            , appthub.trainer_id
                            , appthub.member_id
                            , appt.status
                        FROM appointments appt
                        LEFT JOIN appointmentshub appthub
                            ON appt.appointment_id = appthub.appointment_id
                        WHERE trainer_id = %s
                        ORDER BY date;
                        """,
                        [trainer_id],
                    )
                    result = []
                    for record in db:
                        appointment = self.record_to_appointment_out(record)
                        result.append(appointment)
                    return result
        except Exception:
            return {"message": "Failed to load your appointments."}

    def create(
        self, appointment: AppointmentIn
    ) -> Union[Error, AppointmentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    appt_result = db.execute(
                        """
                        INSERT INTO appointments
                            (date
                            , time
                            , comments)
                        VALUES (
                            %s, %s, %s
                        )
                        RETURNING appointment_id, status;
                        """,
                        [
                            appointment.date,
                            appointment.time,
                            appointment.comments,
                        ],
                    )
                    appt_fetchone = appt_result.fetchone()
                    db.execute(
                        """
                        INSERT INTO appointmentshub
                            (member_id
                            , trainer_id
                            , appointment_id)
                        VALUES (
                            %s, %s, %s
                        );
                        """,
                        [
                            appointment.member_id,
                            appointment.trainer_id,
                            appt_fetchone[0],
                        ],
                    )
                    return self.appointment_in_to_out(
                        appointment_id=appt_fetchone[0],
                        status=appt_fetchone[1],
                        appointment=appointment,
                    )
        except Exception:
            return {"message": "Failed to load your appointments."}

    def get_appointment(
        self, appointment_id: int
    ) -> Optional[Union[AppointmentOut, Error]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT appt.appointment_id
                            , appt.date
                            , appt.time
                            , appt.comments
                            , appthub.trainer_id
                            , appthub.member_id
                            , appt.status
                        FROM appointments appt
                        LEFT JOIN appointmentshub appthub
                            ON appt.appointment_id = appthub.appointment_id
                        WHERE appt.appointment_id = %s
                        ORDER BY date;
                        """,
                        [appointment_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_appointment_out(record)
        except Exception:
            return {"message": "Failed to pull that appointment"}

    def update_appointment(
        self, appointment_id: int, appointment: AppointmentIn
    ) -> AppointmentOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE appointments
                        SET date=%s,
                            time=%s,
                            comments=%s
                        WHERE appointment_id=%s;
                        """,
                        [
                            appointment.date,
                            appointment.time,
                            appointment.comments,
                            appointment_id,
                        ],
                    )
                    result = db.execute(
                        """
                        SELECT appt.appointment_id
                            , appt.date
                            , appt.time
                            , appt.comments
                            , appthub.trainer_id
                            , appthub.member_id
                            , appt.status
                        FROM appointments appt
                        LEFT JOIN appointmentshub appthub
                            ON appt.appointment_id = appthub.appointment_id
                        WHERE appt.appointment_id = %s
                        ORDER BY date;
                        """,
                        [appointment_id],
                    )
                    record = result.fetchone()
                    return self.record_to_appointment_out(record)
        except Exception:
            return {"message": "Failed to pull that appointment"}

    def update_appointment_status(
        self, appointment_id: int, appointment: AppointmentStatusIn
    ) -> AppointmentOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE appointments
                        SET status=%s
                        WHERE appointment_id=%s;
                        """,
                        [appointment.status, appointment_id],
                    )
                    result = db.execute(
                        """
                        SELECT appt.appointment_id
                            , appt.date
                            , appt.time
                            , appt.comments
                            , appthub.trainer_id
                            , appthub.member_id
                            , appt.status
                        FROM appointments appt
                        LEFT JOIN appointmentshub appthub
                            ON appt.appointment_id = appthub.appointment_id
                        WHERE appt.appointment_id = %s
                        ORDER BY date;
                        """,
                        [appointment_id],
                    )
                    record = result.fetchone()
                    return self.record_to_appointment_out(record)
        except Exception:
            return {"message": "Failed to updated appointment status"}

    def delete(self, appointment_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM appointments
                        WHERE appointment_id = %s
                        """,
                        [appointment_id],
                    )
                    return True
        except Exception:
            return False

    def record_to_appointment_out(self, record):
        return AppointmentOut(
            appointment_id=record[0],
            date=record[1],
            time=record[2],
            comments=record[3],
            trainer_id=record[4],
            member_id=record[5],
            status=record[6],
        )

    def appointment_in_to_out(
        self, appointment_id: int, status: str, appointment: AppointmentIn
    ) -> Union[Error, AppointmentOut]:
        old_data = appointment.dict()
        return AppointmentOut(
            appointment_id=appointment_id, status=status, **old_data
        )
