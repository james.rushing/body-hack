from models import CircuitIn, CircuitOut, Error
from queries.pool import pool
from typing import Optional, List, Union
from fastapi import HTTPException, status


class CircuitRepository:
    def create(self, circuit: CircuitIn) -> CircuitOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO circuits
                            (exercise,
                            weight,
                            reps_or_time,
                            workout_id,
                            sets
                            )
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            circuit.exercise,
                            circuit.weight,
                            circuit.reps_or_time,
                            circuit.workout_id,
                            circuit.sets,
                        ],
                    )
                    id = result.fetchone()[0]
                return self.circuit_in_to_out(id, circuit)
        except Exception:
            return {"message": "Failed to create a new circuit."}

    def get_one(self, circuit_id: int) -> Optional[CircuitOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                               exercise,
                               weight,
                               reps_or_time,
                               workout_id,
                               sets
                        FROM circuits
                        WHERE id = %s
                        """,
                        [circuit_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_circuit_out(record)
        except Exception:
            return {"message": "Failed to load your circuit."}

    def delete(self, circuit_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM circuits
                        WHERE id = %s
                        """,
                        [circuit_id],
                    )
                    return True
        except Exception:
            return False

    def update(
        self, circuit_id: int, circuit: CircuitIn
    ) -> Union[CircuitOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE circuits
                        SET exercise = %s,
                            weight = %s,
                            reps_or_time = %s,
                            workout_id = %s,
                            sets = %s
                        WHERE id = %s
                        """,
                        [
                            circuit.exercise,
                            circuit.weight,
                            circuit.reps_or_time,
                            circuit.workout_id,
                            circuit.sets,
                            circuit_id,
                        ],
                    )
                    return self.circuit_in_to_out(circuit_id, circuit)
        except Exception:
            return {"message": "Failed to update your circuit."}

    def get_all_circuits(
        self, workout_id: int
    ) -> Union[Error, List[CircuitOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id,
                               exercise,
                               weight,
                               reps_or_time,
                               workout_id,
                               sets
                        FROM circuits
                        WHERE workout_id = %s
                        ORDER BY id;
                        """,
                        [workout_id],
                    )
                    result = []
                    if db:
                        for record in db:
                            circuit = self.record_to_circuit_out(record)
                            result.append(circuit)
                        return result
                    else:
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="No circuits found.",
                        )
        except Exception:
            return {"message": "Failed to load your circuits."}

    def circuit_in_to_out(self, id: int, circuit: CircuitIn):
        old_data = circuit.dict()
        return CircuitOut(id=id, **old_data)

    def record_to_circuit_out(self, record):
        return CircuitOut(
            id=record[0],
            exercise=record[1],
            weight=record[2],
            reps_or_time=record[3],
            workout_id=record[4],
            sets=record[5],
        )
