from models import TrainerOut, Error
from typing import List, Union, Optional
from queries.pool import pool


class TrainerRepository:
    def get_one(self, trainer_id: int) -> Optional[TrainerOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                             , first_name
                             , last_name
                             , email
                             , role
                        FROM trainers
                        WHERE id = %s;
                        """,
                        [trainer_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_trainer_out(record)
        except Exception as e:
            return ("Exception:", {e})

    def get_all(self) -> Union[List[TrainerOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                             , first_name
                             , last_name
                             , email
                             , role
                        FROM trainers
                        ORDER BY email;
                        """
                    )
                    return [
                        self.record_to_trainer_out(record) for record in result
                    ]
        except Exception as e:
            return ("Exception:", {e})

    def record_to_trainer_out(self, record):
        return TrainerOut(
            id=record[0],
            first_name=record[1],
            last_name=record[2],
            email=record[3],
            role=record[4],
        )
