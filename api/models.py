from datetime import date, time
from jwtdown_fastapi.authentication import Token
from pydantic import BaseModel
from typing import Optional, List


class Error(BaseModel):
    message: str


class HttpError(BaseModel):
    detail: str


class AccountIn(BaseModel):
    email: str
    password: str
    first_name: str
    last_name: str


class AccountOutWithHashedPassword(BaseModel):
    id: int
    email: str
    hashed_password: str
    first_name: str
    last_name: str
    role: str


class AccountOut(BaseModel):
    id: int
    email: str
    first_name: str
    last_name: str
    role: str | None


class AccountOutId(BaseModel):
    id: int


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class MemberIn(BaseModel):
    first_name: str
    last_name: str
    email: str


class MemberInWithHash(BaseModel):
    first_name: str
    last_name: str
    email: str
    hashed_password: str


class MemberOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    email: str
    role: str | None


class MemberOutWithHash(BaseModel):
    id: int
    first_name: str
    last_name: str
    email: str
    hashed_password: str
    role: str | None


class MemberList(BaseModel):
    members: List[MemberOut]


class TrainerOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    email: str
    role: str


class TrainerList(BaseModel):
    trainers: List[TrainerOut]


class WorkoutIn(BaseModel):
    category: str
    personal_or_trainer: str
    trainer: Optional[int]
    date: date
    start_time: time
    end_time: time
    member_id: int


class WorkoutOut(BaseModel):
    id: int
    category: str
    personal_or_trainer: str
    trainer: Optional[int]
    date: date
    start_time: time
    end_time: time
    member_id: int


class WorkoutList(BaseModel):
    personal_workouts: List[WorkoutOut]


class CircuitIn(BaseModel):
    exercise: str
    weight: str
    reps_or_time: str
    workout_id: int
    sets: int


class CircuitOut(BaseModel):
    id: int
    exercise: str
    weight: str
    reps_or_time: str
    workout_id: int
    sets: int


class CircuitList(BaseModel):
    circuits: List[CircuitOut]


class AppointmentIn(BaseModel):
    date: date
    time: time
    trainer_id: int
    member_id: int
    comments: Optional[str]


class AppointmentOut(BaseModel):
    appointment_id: int
    date: date
    time: time
    comments: Optional[str]
    trainer_id: int
    member_id: int
    status: str


class AppointmentStatusIn(BaseModel):
    status: str


class AppointmentList(BaseModel):
    appointments: List[AppointmentOut]
