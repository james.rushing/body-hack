from fastapi.testclient import TestClient
from main import app
from unittest import TestCase
from queries.personal_workouts import WorkoutRepository, WorkoutIn
from models import WorkoutOut
from authenticator import authenticator

client = TestClient(app)


class MockWorkoutRepository:
    def create(self, workout: WorkoutIn):
        return WorkoutOut(
            id=1,
            category="thigh",
            personal_or_trainer="personal",
            trainer=None,
            date="2024-01-29",
            start_time="20:00:00",
            end_time="21:00:00",
            member_id=1,
        )


def mock_try_get_current_account_data():
    return {"id": 1, "email": "james@bodyhack.com"}


class TestPersonalWorkouts(TestCase):
    def test_create_workout(self):
        # Arrange
        app.dependency_overrides[
            authenticator.try_get_current_account_data
        ] = mock_try_get_current_account_data
        app.dependency_overrides[WorkoutRepository] = MockWorkoutRepository

        workout = {
            "category": "thigh",
            "personal_or_trainer": "personal",
            "date": "2024-01-29",
            "start_time": "20:00:00",
            "end_time": "21:00:00",
            "member_id": 1,
        }

        # Act
        response = client.post("/workouts/mine", json=workout)
        workout = response.json()
        print(workout)

        # Assert
        assert response.status_code == 200
        assert workout["category"] == "thigh"

        # Clean up
        app.dependency_overrides = {}
