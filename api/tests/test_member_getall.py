from fastapi.testclient import TestClient
from main import app
from models import MemberOut
from queries.members import MemberRepository

client = TestClient(app)


class MockMemberRepository:
    def get_all(self):
        return [
            MemberOut(
                    id=1,
                    first_name="First1",
                    last_name="Last1",
                    email="last@web.com",
                    hashed_password="fake_password",
                    role="member"
            ),
            MemberOut(
                    id=2,
                    first_name="First1",
                    last_name="Last1",
                    email="last@web.com",
                    hashed_password="fake_password",
                    role="member"
            )
        ]


def test_get_all():
    app.dependency_overrides[MemberRepository] = MockMemberRepository

    response = client.get("/members")
    members = response.json()

    assert response.status_code == 200
    assert len(members["members"]) == 2

    app.dependency_overrides = {}
