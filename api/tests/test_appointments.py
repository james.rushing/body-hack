from fastapi.testclient import TestClient
from main import app
from unittest import TestCase
from queries.appointments import AppointmentRepository
from authenticator import authenticator


client = TestClient(app)


class MockAppointmentRepository:
    def trainer_get_all(
            self,
            trainer_id: int
    ):
        return {}

    def member_get_all(
            self,
            member_id: int,
    ):
        return [
            {
                "appointment_id": "1",
                "date": "2024-02-02",
                "time": "09:00:00",
                "comments": "test",
                "trainer_id": "1",
                "member_id": "1",
                "status": "pending"
            },
            {
                "appointment_id": "2",
                "date": "2024-02-05",
                "time": "09:00:00",
                "comments": "test",
                "trainer_id": "1",
                "member_id": "1",
                "status": "accepted"
            }
        ]


def mock_try_get_current_account_data():
    return {"id": 1, "email": "eddie@bodyhack.com"}


class TestAppointments(TestCase):
    def test_member_get_all(self):
        app.dependency_overrides[
            authenticator.try_get_current_account_data
            ] = mock_try_get_current_account_data
        app.dependency_overrides[
            AppointmentRepository
            ] = MockAppointmentRepository

        response = client.get("/members/mine/appointments")
        appointment = response.json()
        print(appointment)

        assert response.status_code == 200
        assert len(appointment["appointments"]) == 2
        assert appointment["appointments"][0]["appointment_id"] == 1
        assert appointment["appointments"][1]["appointment_id"] == 2
        assert appointment["appointments"][0]["member_id"] == 1
        assert appointment["appointments"][1]["member_id"] == 1

        app.dependency_overrides = {}
