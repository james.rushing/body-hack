from fastapi.testclient import TestClient
from main import app
from queries.trainers import TrainerRepository
from models import TrainerOut


client = TestClient(app)


class MockTrainerRepository:
    def get_all(self):
        return [
            TrainerOut(
                id=1,
                first_name="Edward",
                last_name="Doe",
                email="EdwardDoe@bodyhack.com",
                role="trainer"
            ),
            TrainerOut(
                id=2,
                first_name="Lily",
                last_name="Collins",
                email="LilyCollins@bodyhack.com",
                role="trainer"
            )
        ]

    def get_one(self, id: int):
        return {
            "id": id,
            "first_name": "Emily",
            "last_name": "Bond",
            "email": "EmilyBond@bodyhack.com",
            "role": "trainer"
        }


def test_get_all_trainers():
    # Arrange
    app.dependency_overrides[TrainerRepository] = MockTrainerRepository

    # Act
    response = client.get("/trainers")

    # Assert
    assert response.status_code == 200
    trainers = response.json()
    print(trainers)
    assert len(trainers["trainers"]) == 2
    assert trainers["trainers"][0]["id"] == 1
    assert trainers["trainers"][1]["last_name"] == "Collins"

    # Clean up
    app.dependency_overrides = {}


def test_get_one_trainer():
    # Arrange
    app.dependency_overrides[TrainerRepository] = MockTrainerRepository
    id = 3

    # Act
    response = client.get(f"/trainers/{id}")

    # Assert
    assert response.status_code == 200
    trainer = response.json()
    print(trainer)
    assert trainer["id"] == 3
    assert trainer == {
            "id": 3,
            "first_name": "Emily",
            "last_name": "Bond",
            "email": "EmilyBond@bodyhack.com",
            "role": "trainer"
        }

    # Clean up
    app.dependency_overrides = {}
