steps = [
    [
        """
        -- Up: Dropping FK constraint on trainer_id in personal workouts
        ALTER TABLE personal_workouts
        DROP CONSTRAINT IF EXISTS trainer;
        """,
        """
        -- Down: Adding constraint to trainer
        ALTER TABLE personal_workouts
        ADD CONSTRAINT trainer;
        """,
    ],
    [
        """
        -- Up: Dropping FK constraint on member_id in personal workouts
        ALTER TABLE personal_workouts
        DROP CONSTRAINT IF EXISTS member_id;
        """,
        """
        -- Down: Adding constraint to member_id
        ALTER TABLE personal_workouts
        ADD CONSTRAINT member_id;
        """,
    ],
    [
        """
        -- Up: Adding new constraint and redefining FK to the new PK
        ALTER TABLE personal_workouts
        ADD CONSTRAINT fk_trainer_id
        FOREIGN KEY (trainer) REFERENCES trainers(id) ON DELETE CASCADE;
        """,
        """
        -- Down: Removing constraint and reverting to old reference
        ALTER TABLE personal_workouts
        DROP CONSTRAINT fk_trainer_id
        FOREIGN KEY (trainer) REFERENCES trainers(trainer_id);
        """,
    ],
    [
        """
        -- Up: Adding new constraint and redefining FK to the new PK
        ALTER TABLE personal_workouts
        ADD CONSTRAINT fk_member_id
        FOREIGN KEY (member_id) REFERENCES members(id) ON DELETE CASCADE;
        """,
        """
        -- Down: Removing constraint and reverting to old reference
        ALTER TABLE personal_workouts
        DROP CONSTRAINT fk_member_id
        FOREIGN KEY (member_id) REFERENCES members(member_id) ON DELETE CASCADE;
        """,
    ],
]
