steps = [
    [
        """
        CREATE TABLE IF NOT EXISTS members (
        member_id SERIAL PRIMARY KEY NOT NULL,
        first_name VARCHAR(100) NOT NULL,
        last_name VARCHAR(100) NOT NULL,
        email VARCHAR(100) NOT NULL UNIQUE,
        hashed_password VARCHAR(500) NOT NULL,
        role VARCHAR(50) DEFAULT 'member'::VARCHAR
        );
        """,
        """
        DROP TABLE members;
        """
    ]
]
