steps = [
    [
        """
        -- "Up" migration: Change member_id to id.
        ALTER TABLE members
        RENAME COLUMN member_id TO id;
        """,
        """
        -- "Down" migration: Revert PK name in members table.
        ALTER TABLE members
        RENAME COLUMN id TO member_id;
        """
    ],
    [
        """
        -- "Up" migration: Change trainer_id to id
        ALTER TABLE trainers
        RENAME COLUMN trainer_id TO id;
        """,
        """
        -- "Down" migration: Revert PK name in trainers table.
        ALTER TABLE trainers
        RENAME COLUMN id to trainer_id;
        """
    ],
    [
        """
        -- Up: dropping FK constraint on member_id.
        ALTER TABLE appointmentshub
        DROP CONSTRAINT IF EXISTS member_id;
        """,
        """
        -- Down: add constraint to member_id
        ALTER TABLE appointmentshub
        ADD CONSTRAINT member_id
        """
    ],
    [
        """
        -- Up: Adding new constraint and redefining the FK to the new PK
        ALTER TABLE appointmentshub
        ADD CONSTRAINT fk_member_id
        FOREIGN KEY (member_id) REFERENCES members(id) ON DELETE CASCADE;
        """,
        """
        -- Down: removing constraint and reverting to old reference
        ALTER TABLE appointmentshub
        DROP CONSTRAINT IF EXISTS fk_member_id
        FOREIGN KEY (member_id) REFERENCES members(member_id) ON DELETE CASCADE;
        """
    ],
    [
        """
        -- Up: dropping FK constraint on trainer_id
        ALTER TABLE appointmentshub
        DROP CONSTRAINT IF EXISTS trainer_id;
        """,
        """
        -- Down: adding constraint to trainer_id
        ALTER TABLE appointmentshub
        ADD CONSTRAINT trainer_id;
        """
    ],
    [
        """
        -- Up: Adding new constraint and redefining the FK to the new PK
        ALTER TABLE appointmentshub
        ADD CONSTRAINT fk_trainer_id
        FOREIGN KEY (trainer_id) REFERENCES trainers(id) ON DELETE CASCADE;
        """,
        """
        -- Down: Removing constraint and reverting to old reference
        ALTER TABLE appointmentshub
        DROP CONSTRAINT fk_trainer_id
        FOREIGN KEY (trainer_id) REFERENCES trainers(trainer_id) ON DELETE CASCADE;
        """
    ]
]
