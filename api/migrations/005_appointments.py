steps = [
    [
        """
        CREATE TABLE IF NOT EXISTS appointments (
        appointment_id SERIAL PRIMARY KEY NOT NULL,
        date DATE NOT NULL,
        time TIME NOT NULL,
        status VARCHAR(100) DEFAULT 'pending'::VARCHAR,
        comments VARCHAR(1000)
        );
        """,
        """
        DROP TABLE appointments;
        """
    ]
]
