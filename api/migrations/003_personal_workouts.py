steps = [
    [
        """
        CREATE TABLE IF NOT EXISTS personal_workouts (
        id SERIAL PRIMARY KEY NOT NULL,
        category VARCHAR(100),
        personal_or_trainer VARCHAR(100),
        trainer INT NULL REFERENCES trainers(trainer_id),
        date DATE,
        start_time TIME,
        end_time TIME,
        member_id INT REFERENCES members(member_id) ON DELETE CASCADE
        );
        """,
        """
        DROP TABLE personal_workouts;
        """
    ]
]
