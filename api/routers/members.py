from fastapi import APIRouter, Depends, Response, status, HTTPException
from models import (
    MemberOut,
    MemberList,
    MemberIn,
    Error,
)
from typing import Union, Optional
from queries.members import (
    MemberRepository,
)
from authenticator import authenticator


router = APIRouter()


@router.get("/members", response_model=Union[MemberList, Error])
def get_all(
    repo: MemberRepository = Depends(),
):
    result = repo.get_all()
    return {"members": result}


@router.put(
    "/members/{member_id}",
    response_model=Union[MemberOut, Error],
    tags=["Members"],
)
def update_member(
    member_id: int,
    member: MemberIn,
    repo: MemberRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
) -> Union[MemberOut, Error]:
    if account_data and account_data["id"] == member_id:
        return repo.update(member_id, member)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You do not have permission to edit that record.",
        )


@router.put(
    "/members/{member_id}/change-password",
    response_model=Union[bool, Error],
)
def update_password(
    member_id: int,
    password: str,
    repo: MemberRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
) -> bool:
    if account_data and account_data["id"] == member_id:
        hashed_password = authenticator.hash_password(password)
        return repo.update_password(member_id, hashed_password)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You do not have permission to edit that record.",
        )


@router.get(
    "/members/{member_id}",
    response_model=Union[Optional[MemberOut], Error],
    tags=["Members"],
)
def get_one_member(
    member_id: int,
    response: Response,
    repo: MemberRepository = Depends(),
) -> MemberOut:
    member = repo.get_one(member_id)
    if member is None:
        response.status_code = 404
    return member


@router.delete(
    "/members/{member_id}", response_model=Union[bool, Error], tags=["Members"]
)
def delete_member(
    member_id: int,
    repo: MemberRepository = Depends(),
    account_data: Optional[dict] = Depends(
        authenticator.try_get_current_account_data
    ),
) -> bool:
    member = repo.get_one(member_id)

    if account_data is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You must be logged in to do that.",
        )
    if member is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Unable to locate member record.",
        )
    if account_data and member and account_data["id"] == member_id:
        return repo.delete(member_id)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You do not have permission to delete that record.",
        )
