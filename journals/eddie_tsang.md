# Daily Journal

## 9 February 2024

-   Adding css styling and component adjustment to the following pages:
    -   home
    -   CircuitsModal
    -   CreateAppointmentForm
    -   CreatePersonalWorkoutForm
    -   MyWorkoutsList
    -   trainerHome
    -   SignupPage
    -   personalWorkoutsApi
-   Adding css styling
    -   homepage.css
    -   index.css

## 8 February 2024

-   For MyWorkoutsList
    -   added styling classes to match homepage
    -   added deletion for workout
-   For CreatePersonalWorkoutForm, added styling classes to match homepage

## 7 February 2024

-   Pair programming With James, we got edit profile to be full functional and added a button toggle to populate change password input
-   Pair programming With James, we got the fitlers to be full functional for MyWorkoutsList

## 6 February 2024

-   With help from James, made progress for filters on the MyWorkoutsList page.

## 5 February 2024

-   Split into pair programing with Yaelin.
-   Help Yaelin with debuggin when she drove.

## 2 February 2024

## 1 February 2024

-   As driver, researched and implemented mult-select for fitlers in MyWorkoutsList.
-   As group, resolved merge conflicts within the repo.
-

## 31 January 2024

-   As passenger, help debug when James was driver for authnication front end.
-   As passenger, help debug when Yaelin was driver as we try to make a
    migrations for inserting data. We ran into bug where would would be able
    to get the password from hashpassword.
-   As passegner, help debug when Elaine was driver to add create trainer
    endpoint.
-   I started working on MyWorkoutsList component.

## 30 January 2024

-   Completed a test for appointments.
-   As driver, started the nav.js component.
-   Further dicussion, we would not use redux and further deep dive into how to
    authenicate users and what would be the appropriate path.
-   As passenger, help debug when James was driver for authnication front end.

## 29 January 2024

-   Help debug as passenger, Elaine was driver for us to checked endpoint to make
    sure they all work with authenication and debug unit testing.
-   I started the test file for appointments.
-   Help debug as passenger, Yaelin was driver for creatig and debug unit
    testing for workout.
-   Help debug as passenger, James was driver as we discuss modification for models.

## 26 January 2024

-   As passenger, we refactor code where all the json format happens in the
    router endpoints.

## 25 January 2024

-   As driver, finish up appointments endpoint functionality.
-   As passenger, debug code after merge quest and authentication.

## 24 January 2024

-   As driver, coded out the queries/appointments and routers/appointments for
    getting trainer's and members list appointments and creating an
    appointment for members.
-   Discussed how appointment endpoints would be like and how to be querying
    for the data since it would require joinning two tables.
-   Helped debug coding that was being used for logged in authentication.

## 23 January 2024

-   As passenger, help with debugging code for endpoints for personal_workouts
    and circuit.
-   Participated in discussion on how tables info would be used with frontend.
-   I will working on the appointments endpoint , moving the pydantic models
    into models.py, and relocating the tags into main.py.

## 22 January 2024

-   As passenger, help with debugging code for endpoints for members and
    trainers.
-   Participated in discussion on how tables info would be used with frontend.

## 19 January 2024

-   As passenger, help with debugging code for authenication.
-   Discussed implementation of code.
-   Get help and helped each other get code working on local machine.

## 18 January 2024

-   Driver to start code for Authenication.
-   Discuss implement of how authenication would work with our app.
-   Review videos for authenication.

## 17 January 2024

-   Help code migration for database as navigators.

## 16 January 2024

-   Set up GitLab repo on local machine.
-   Added my journal.
