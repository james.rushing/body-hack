# Daily Journal

## 16 January 2024

-   Set up GitLab repo.
-   Updated requirements to include black and some auth things.
-   Explored git commands.
-   Updated the docker-compose to include PostgreSQL database and added a .env to hold the username and password for the database.

## 17 January 2024

-   Driver for coding migrations. Set up database tables. We broke out circuits from personal workouts and made it a separate table, with workouts as a FK. This allows us to have multiple circuits per workout.
-   Long discussion about authentication and whether we need to use the 'role' to determine which components to show on the front end, or whether the authenticated email would work. We agreed to try using the email (with the concept that trainers' emails will be @bodyhack.com) rather than the role and see if we can figure out how to code it.

## 18 January 2024

-   Added a circuithub table to handle the many-to-many (MTM) relationship between personal_workouts and circuits
-   Added PGAdmin functionality
-   Refactored the personal_workouts and circuits to properly relate the tables to the MTM hubs (because FK to workout table was unnecessary and generated a circular reference).
-   Generated an updated ERD to show current table relationships
-   fixed the issue with FastAPI container crashing on startup (added wait_hosts, wait_hosts_before, and wait_timeout to FastAPI container environment, and added depends_on so it will wait until the db is up)

-   With Eddie driving, worked on authentication and setting up jwtdown to accept logins from existing users.

-   In the evening, finished up the authentication with signup, login, and logout functionality.

## 19 January 2024

-   Driver for auth. Added error catching for duplicate account creation; fixed various bugs with the queries. Refactored database to remove circuithub and more accurately reflect the one-to-many relationship between personal_workouts and circuits.

-   Added error handling for incorrect email/password on login.
    ** Learn how to handle multiple sessions!

## 22 January 2024

** With Elaine driving, completed API endpoints for Members and Trainers CRUD functionality. Trainers have only get_all and get_one (because there is currently no way to add trainers through the app).

** Spent considerable time trying to figure out how to integrate GitLab issues into our workflow. Eddie and Yaelin will try to figure it out; currently we will reference the user story number in the MR to maintain a record.

** Ongoing issues:

-   If you are currently logged in, it is possible to attempt to log in again; while it doesn't log you in, it does lock up the server for 30 seconds until it times out.
-   We would like to lock down the API endpoints unless someone is logged in. Possible solution: add something like

```python
if get_token() is "null":
raise HTTPException(
    status_code=status.HTTP_403_FORBIDDEN,
    detail="You must be logged in to do this operation."
)
```

to each method. Or better, make a new function to check and call that function when each endpoint is hit.

## 23 January 2024

** A better solution to locking down the endpoints will be to use JWTdown's inbuilt `python try_get_current_account_data`:

```python
from typing import Optional
from authenticator import authenticator
from fastapi import APIRouter

router = APIRouter()


@router.get("/api/things")
async def get_things(
    account_data: Optional[dict] = Depends(authenticator.try_get_current_account_data),
):
    if account_data:
        return personalized_list
    return general_list
```

** Figure out a way to autopopulate the update methods with member_id (rather than letting the user define it).

** With Yaelin driving, implemented the endpoints for personal_workouts and circuits.

** Added authenticator to members update method, which checks to ensure that the record being updated is the member's record.
** Added error checking and authentication to member delete method, which checks to make sure that the account exists and that it is the member's account.
** Would like to add functionality to automatically log the user out if the account is deleted.

## 24 January 2024

** Worked on further developing and implementing login checks for endpoints
** With Eddie driving, implemented queries/appointments and routers/appointments

-   developed queries for pulling from appointments, appointmenthub, and trainer and member tables
    ** Marce talked me through the automatic logout idea, and turns out it's going to be way easier to just redirect and logout from the front end. Possibly use expiring tokens?

## 25 January 2024

** With Eddie driving, finished the appointments endpoints and debugged the routes.
** With me driving, added login verification to endpoints; added getMemberIdFromEmail to use for filtering personal workouts/mine

## 26 January 2024

** Out sick.

## 27 January 2024

** Refactored backend authentication: - Removed GetEmailFromId and GetIdFromEmail, and refactored the authenticator to provide both when called
** Changed member and trainer models to simplify db queries - Changed database models: trainer_id and member_id are now "id" in their respective tables. - Added migration up and down for those changes and all related FKs
** Added VITE_API_HOST, removed boilerplate from App.jsx

** Helped debug tests for Elaine and Yaelin. Eddie will work on his tests and present tomorrow.
** Added models for MemberInWithHash and MemberOutWithHash, and removed hashed password from MemberIn and MemberOut
** Adjusted routes that used those models to reflect new relationships
** Added VITE_API_HOST, removed boilerplate from App.jsx

## 29 January 2024

** Completed Yaelin's unit test with her driving.
** Removed hashed password from routes that don't actually need that information

## 30 January 2024

** Broke out change_password to a separate route (members/{member_id}/change-password) and separate function in MembersRepository
** Eddie driving started coding front-end Redux slices and started work on the Nav base component.
** Yaelin driving worked on Nav component and expanded Redux slices.

** Long conversation about how we can: - Verify users are logged in - Filter views based on the roles of logged in users - Prevent users from accessing pages if they aren't logged in and/or are not the appropriate role.

** Without using Redux, the solution is difficult. We can't set a global state variable to call. So possible solutions are: - Do a fetch to /token to get the member id (which is part of the token response, but doesn't get saved in getToken) in each component, and use that to set a state for the component with the role. - We CAN use getToken to determine whether the user is logged in, and we can simply call that from getToken, because it's stored in state in jwtdown - Do a fetch upon login to get the role (this would require getting the /token, then taking the id to get the role from the get_member or get_trainer endpoint, and use that role to set a state) and then prop-drill that into each component - Or create an ID token that will store the role in the cookie jar (which will require significant research)

** Finished initial frontend login page and basic css styling.

** Worked on setting up getting the role from the token. I can retrieve the token, but for some reason it resets to null before it finishes? There are 6 token fetches every time I refresh, and numbers 3 and 6 are null but the others are the token. ???

## 31 January 2024

** Resolved the issue with pushing role. Made a separate component to call in order to get the role; the issue was trying to use ${API_HOST}. Not sure why - when I hard-coded the route to the same location, it then worked.

** Still getting a weird syntax error "SyntaxError: Unexpected token '<', "</!doctype "... is not valid JSON". Doesn't seem to break the program (yet), but not sure where that's coming from. (Actually fixed this, but I'm not really sure how. Once the token started coming in properly, this went away.)

** Changed the hard-coded paths for fetchRole paths to environment variables and updated the docker-compose to reflect those.

** Frontend auth works, and the redirect works, but it doesn't automatically refresh upon redirect. Riley points out that what I was trying to do, which is pass the state in as a prop from the login page, is really not possible. We will almost certainly need to do API calls in every component to get the role, if we go that way.

** So tonight I will explore refactoring this function (at least) into Redux and see if it's as difficult as my team thinks. If I can get it to work, we'll refactor the auth and see where we can implement it.

** Finished refactoring the roles to Redux. Not too terrible, though it took a lot of chats with ChatGPT, who seems to take a perverse joy in giving me an answer that is always at least 20% completely wrong. Added a retry counter to the tokens; it was breaking when initializing because of the lag between logging in and jwtdown issuing the token to the backend.

## 01 February 2024

** Added logout functionality to Nav component, and integrated with Redux slice. It will logout then reset the state and navigate to the home page.

** Added localStorage to maintain state upon page refresh.

** Dear god finally fixed that login issue where it broke because the token hadn't had time to become available before it tried to fetch it. (Basically, there's a lag between when jwtdown issues the token and when it's available from /token. That, and the fact that 'null' is a 200 response, meant that we weren't getting the proper role after login unless we refreshed the page.) Solved by putting the fetchRole in a useEffect and having it depend on the token being available. (Thanks to Brendan getting me about 70% there with his fix for it.)

** TODO: set the ID as a global state upon login

** Got bored after class, so I did some basic styling of the buttons and the Main Page. Figured out how to make the background rotate through a list of different pictures. I was going to do videos, but I think that's probably going to be a little laggy and honestly, the pictures look pretty cool.

** Had an AI make a couple of custom logos and then turned it into the navbar-brand button. Fun exercise.

\** Navbar buttons show based on your role. Getting the buttons to justify correctly is ... unintuitive. I think it's because even though you're not rendering the buttons in JSX (due to the conditional), they still *exist\*, and therefore still take up space in the container. Which means "justify-content-right/end" or "justify-content-middle" don't always do what you think they will, because "right" and "middle" don't mean what they look like they mean on the screen. Working so far, but I wouldn't expect it to hold up for extreme screen resizes.

## 02 February 2024

** Groundhog day! Don't drive angry, Phil.

** New slice sets a global state 'userId' and saves it to localStorage.

** Todo today: - Only render the background pictures on the main page; other pages should have different backgrounds - Does role/userId state remain if you shut down the container? Probably, since it's locally stored. Maybe set up a check on the main page to see if you're logged in, and if not reset the state? - Frame out member and trainer "home" pages.

## 04 February 2024

** Lots of stuff here. - Added member home page. It loads appointments filtered by memberId which is stored in a slice. - Trainer and member fullNames are now stored as global variables in the state.fullName store. - Create appointment does not use Redux, so I can't use tags there to refetch, but Accept and Reject on the trainer page do, so it refetches the appointments (which have not been listed yet) - Added new backgrounds for the trainer and member pages - Nav buttons on the member pages are up and functioning - But not the trainer page yet. - Added some icons to the /public for possible future use

## 05 February 2024

** Things to do: use the isError on member and trainer pages to pop up an error modal
** Add appointments list for trainers (done)
** Fixed the auto refetch for members appointments (tags need to be in the SAME ApiSlice, thanks for nothing Redux docs)
** Refactored createAppointment to use Redux mutation, it properly redirects and updates the appointments list now
** Fixed backgrounds so they remain fixed while the content can scroll over it
** Added styling to footer
** Added formatting so that dates and times render in more human-friendly format
** Fixed paths for backgrounds so that certain components do not attempt to make GET requests to those background files.
** FINALLY fixed that damn login issue. Wrote a mutation to handle the login function and finally thought to check what actual data type the endpoint needed (application/x-www-form-urlencoded, NOT json).

## 06 February 2024

** Spent most of the day with Eddie driving, trying to get the workout and circuit pages to work. Mostly they do now, although I'm still working on getting the circuit modal to support in-line editing, and yet again, I can't tell what information the mutation is sending to the backend, so when it comes back "unprocessable entity", I don't really know why.

    - Update: I do know why. Because RTK Queries take ONE argument. If you want to pass more, you have to make them into an object that you destructure in the slice.

** Something in the formatting of the footer has broken the backgrounds in the rest of the app. I'm really not sure what happened or how, but I suspect that it has to do with the universal styling someone put in the App.jsx.

** The circuit endpoints were incorrectly configured, which was messing up the routes for put, post, delete circuits. (If you have a dynamic link in the route, but that doesn't match the input requirements for the endpoint, FastAPI will render that as an object rather than a dynamic link. So either use it or don't put it.)

## 07 February 2024

** Navigated for Eddie to fix his components, we had to refactor to use Redux on the edit profile. Added a button toggle to populate the change password inputs, and fixed the filters on MyWorkoutList.

** Added appointment lists on trainer and member pages to show 1-hour ranges.

** Fixed a date translation issue where the dates on the screen were one day off from the days in the data. (If you render dates that are in traditional datetime format YYYY-MM-DD, when you render it, it will be a day off. But if you translate it to YYYY/MM/DD, it will render correctly.)

** The change password API endpoint is not secure; it transmits the username and password data in the url as a query. Will need to change the backend endpoint to take a body (probably stretch goal).

## 08 February 2024

** Bug noted that if you are logged in, and jwtdown logs you out, you have no way of knowing until you try to fetch something and get a 401. Need to find a way to have the frontend check whether there's a real token, and if not, redirect you to the home page.

** Bodymap is done. It uses react-body-highlighter and takes in an object with muscle types and frequency and returns an SVG with the muscle groups highlighted. This dynamically changes based on the last seven days workouts that you've entered.

** Adjusted the member and trainer appointments to only list the last 7 days, and the member appointments list rows are color coded based on whether they're pending, accepted, or rejected.

** Still working on the stupid login check thing. Custom hooks work, but you can't make them reactive enough. It will check when the component is mounted, but NOT when it's rendered (because React uses cached versions of components unless you tell it to change. But in this case, there's no state that will change when the token is unavailable, because jwtdown doesn't tell you that it's expired, it just returns a null. So the first time you know you're not logged in is when you get a 401.)

## 09 February 2024

** Fixed the login thing (sort of). You can use api.util.invalidateTags to invalidate the tag and force a recache. So now if you try to get appointments on memberhome, and you get a 401, it will log you out and invalidate the MemberAppointments tag. That doesn't solve all the issue, but it solves some of it.

** Did some basic styling and advising of other people, but mostly worked on that. Glad it's Friday.
